----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:    Andres Ruiz
-- Date:    2/01/2021
-- Course:  CSCE 436
-- File:    multiple_tb.vhdl
-- HW:      Homework 3
-- Purp:    test bench for multiple.vhdl
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity multiple_tb is 
end entity multiple_tb;

architecture behavior of multiple_tb is

	----------------------------------------------------------------------
	-- These signal names must match the names of the I/O markers
	----------------------------------------------------------------------
	component multiple
        port(	I0, I1, I2, I3, I4, I5, I6, I7 :	in std_logic; 
					f:   		out std_logic);
	end component;
  
  
	signal s1, s2, s3, s4, s5, s6, s7, s8, s9: std_logic;
	
	signal testVector: std_logic_vector(7 downto 0);
	CONSTANT TEST_ELEMENTS:integer:=10;
	
	SUBTYPE INPUT is std_logic_vector(7 downto 0);
	TYPE TEST_INPUT_VECTOR is array (1 to TEST_ELEMENTS) of INPUT;
	SIGNAL TEST_IN: TEST_INPUT_VECTOR := (	"01000100", "01100110", "01010101", "10011001", "00010001", "01101010", "00001111", "11110000", "01101001", "01111110");

    SUBTYPE OUTPUT is std_logic;
	TYPE TEST_OUTPUT_VECTOR is array (1 to TEST_ELEMENTS) of OUTPUT;
	SIGNAL TEST_OUTPUT: TEST_OUTPUT_VECTOR := ('1', '1', '1', '1', '1', '0', '0', '0', '0', '0');

	SIGNAL i : integer;		

begin

	----------------------------------------------------------------------
	-- Instantiate the Unit Under Test (UUT)
	----------------------------------------------------------------------
	UUT:	multiple port map (
	   I0 => s1,
	   I1 => s2,
	   I2 => s3,
	   I3 => s4,
	   I4 => s5,
	   I5 => s6,
	   I6 => s7,
	   I7 => s8,
	   f => s9);

	   
        s1 <= testVector(0);
        s2 <= testVector(1);
        s3 <= testVector(2);
        s4 <= testVector(3);
        s5 <= testVector(4);
        s6 <= testVector(5);
        s7 <= testVector(6);
        s8 <= testVector(7);
        
		
	process
	begin
	
	for i in 1 to TEST_ELEMENTS loop
		-----------------------------------------
		-- Parse out the bits of the test_vector
		-----------------------------------------
		testVector <= test_in(i);
		wait for 10 ns; 
		assert s9 = test_output(i)
 		 		report "Error with input " & integer'image(i) & " in multiple circuit "
				severity failure;
	end loop;
	
	---------------------------
	-- Halt the simulator
	---------------------------
    std.env.finish;
			
	end process tb;

end architecture behavior;

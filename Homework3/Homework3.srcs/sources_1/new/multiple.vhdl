----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:    Andres Ruiz
-- Date:    2/01/2021
-- Course:  CSCE 436
-- File:    multiple.vhdl
-- HW:      Homework 3
-- Purp:    light up led if input is a multiple of 17
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity multiple is
        port(	I7, I6, I5, I4, I3, I2, I1, I0: std_logic; 
		f	: out std_logic);
end multiple;

architecture behavior of multiple is

    signal most_significant_4: std_logic_vector(3 downto 0);
    signal least_significant_4: std_logic_vector(3 downto 0);
    
begin
	
	most_significant_4 <= I7 & I6 & I5 & I4;
	least_significant_4 <= I3 & I2 & I1 & I0;
	
	f <= '1' when most_significant_4 = least_significant_4 else 
	     '0';

end behavior;

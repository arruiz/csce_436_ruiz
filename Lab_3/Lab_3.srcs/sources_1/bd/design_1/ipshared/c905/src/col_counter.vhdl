----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/23/2021
-- Course:    CSCE 436
-- File:      col_counter.vhdl
-- Lab:       Lab 2
-- Purp:      column counter file for lab 2
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;


entity col_counter is
        port(	clk, reset_n: in std_logic; 
		ctrl: in std_logic;
		col_number: out unsigned(9 downto 0);
		col_roll: out std_logic);
end col_counter;

architecture behavior of col_counter is
	signal process_col_number: unsigned (9 downto 0);

begin
	
	
	-----------------------------------------------------------------------------
	--		ctrl
	--		00			hold
	--		01			count up mod 10
	--		10			load D
	--		11			synch reset
	-----------------------------------------------------------------------------
	process(clk)
	begin
	    -----------------------------------------------------------------------------
        --		ctrl
        --		0			hold
        --		1			count up mod 10
        -----------------------------------------------------------------------------
	    -- Least significant
		if (rising_edge(clk)) then
			if (reset_n = '0') then
				process_col_number <= (others => '0');
			elsif ((process_col_number < 799) and (ctrl = '1')) then
				process_col_number <= process_col_number + 1;
			elsif ((process_col_number = 799) and (ctrl = '1')) then
				process_col_number <= (others => '0');
			end if;
		end if;
		
		-- Most significant
--		if (rising_edge(clk)) then
--			if (reset = '0') then
--				processQ1 <= (others => '0');
--				rollSynch <= '0';
--			elsif ((processQ1 = 4) and (ctrl = '1') and rollCombo = '1') then
--				processQ1 <= (others => '0');
--			elsif ((rollCombo = '1') and (ctrl = '1')) then
--				processQ1 <= processQ1 + 1;
--			end if;
--		end if;
	end process;
 
	-- CSA
	col_roll  <= '1' when (process_col_number = 799) else '0';
	col_number <= process_col_number;
	
end behavior;

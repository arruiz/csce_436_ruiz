# Lab 3 - Software control of a datapath

## By Andres Ruiz

## Table of Contents (not required but makes things easy to read)
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose  

The purpose of this lab was to integrate UART functionality into our Lab 2 oscilloscope and add additional functionality. To do this, I had to create an oscilloscope ip and connect it to the microblaze. Then, I had to modify the slave registers to fit the needs of the progams functionality. Then in the SDK, I had to modify the slave registers based off inputs from the UART to update the dispaly. This allowed the functionality to transfer from lab 2. The additional requirements needed a few more slave register assignments and connections in the SDK. With some logic that is disucussed later in the lab report, I was able to acheive full functionality of this lab. 

### Preliminary design

#### Gate Check 1

During Gate Check 1, the main focus was to be able to display the scopeface by creating an IP. This required adding all the Lab 2 files into a new IP and setting the unconnected singals as external. Inside the SDK, I just added a print statement to ensure the project was working as intended. 

![Base Tera Term](images/basicTeraTerm.png)
##### Figure 1: Base Tera Term for Gate Check 1
 
![Base Tera Term](images/gate_check1_scopeface.jpg)
##### Figure 2: Scopeface display after Gate Check 1

#### Gate Check 2

During Gate Check 2, the main focus was to use the slave registers to transfer data between the two projects. Inside the IP project, we had to assign the slave registers to what we planned to put into them. For example, I used `slv_reg0` to store the external write address and transfer the information from the SDK to the `.vhdl` files. After grabbing the data for trigger volt and trigger time, I was able to create two new signals for the datapath that took the trigger volt and trigger time from the SDK and displayed those values. To clean up the datapath, I no longer needed the button debouncing or movements so I removed that code. 

[Gate Check 2 Video](https://www.youtube.com/watch?v=zBuGp-xLJMY)

#### Tera Term Menu

The first step I took after the gate check was developing the full tera term menu. This allowed me to see what needed to be done. I used the one Professor Falkinburg created to help guide me to the one I designed. Figure 3 shows the Tera Term Menu I created.

![Full Tera Term](images/fullTeraTerm.png)
##### Figure 3: Full Tera Term Menu

### Software flow chart or algorithms

The architecute used was given in the Lab 3 Write Up and is shown in figure 4. It shows how the `.vhdl` files and the `.c` files communicate through the Microblaze.

![Architecture](images/lab3architecture.png)
##### Figure 4: Lab 3 Architecture

The only algorithm used in this lab was how to print based off the trigger volt and trigger time. This is done by locatating the intersection of trigger volt and the wave (same algorithm used in lab 2). However, this time instead of starting at the left side of the scopeface, I started at the location of trigger time. This allows me to find the next (rising or falling) intersection. Using this value, I subtracted trigger time from it. This is how far trigger time is currently from the position of the intersection. To move the wave, I must delay printing the wave by that many indices. This arrive at the formula given in `intersection location - trigger time`

Slave Register Map used during implementation:
```
Slave Register Map
0 : External Write Address
1 : External Write Enable
2 : External Select
3 : Left Bus Out
4 : Right Bus Out
5 : External Left Bus
6 : External Right Bus
7 : Flag Clear
8 : FlagQ
9 : Trigger Volt
10: Trigger Time
11: Channel 1 Enable / Disable
12: Channel 2 Enable / Disable
```

#### Pseudocode:
The following pseudocode explains how to calculate where to start printing.

```
bus_index
WHILE NOT ( next_bus > trigger_volt and current_bus < trigger_volt )
	current_bus = next_bus
	next_bus = LBus[bus_index]
	bus_index = bus_index + 1
```

The `LBus` in the psudocode above describes all 1024 values that are currently stored in the LBus display. The `bus_index` variable describes the current index the loop is looking at.

#### Code:
The following code snippet shows how we find the location of the intersection of trigger volt and the falling edge. 
```
	/*--------------------------------------------------------------------
	Name: Andres Ruiz
	Date: 3/19/2021
	Course:	CSCE436
	File: lab3.c
	HW:	Lab 3

	Purp:A brief description of what this program does and
		the general solution strategy.

	Academic Integrity Statement: I certify that, while others may have
	assisted me in brain storming, debugging and validating this program,
	the program itself is my own work. I understand that submitting code
	which is the work of other individuals is a violation of the honor
	code.  I also understand that if I knowingly give my original work to
	another individual is also a violation of the honor code.
	-------------------------------------------------------------------------*/
...
void myISR(void) {

	// Store L and R bus off every ready signal
	storedLBus[busAddress] = Xil_In16(oscopeLBusOutReg);
	storedRBus[busAddress] = Xil_In16(oscopeRBusOutReg);

	busAddress++;						// Incrementing the external write address

	if (busAddress > 1023) { // If at the max count then reset
		busAddress = 20;
		// If waiting on display, update display and reset display variable
		if (displayOnFilledBuffer) {
			updateWave();
			displayOnFilledBuffer = 0;
		}
	}


	Xil_Out8(oscopeFlagClearReg, 0x01);					// Clear the flag and then you MUST
	Xil_Out8(oscopeFlagClearReg, 0x00);					// allow the flag to be reset later

}
```
The myISR function is called every time the ready signal is activated. This function stores the L and R bus that were just outputed by the audio codec wrapper. It also resets the write counter if it reaches its maximum limit. After the ISR is done, the final thing to do is to clear the flag register. 

```
...

	int currentBusIndex = triggerTimeLocation + 2;
	int nextBus = triggeredBus[triggerTimeLocation + 1];
	int currentBus = triggeredBus[triggerTimeLocation];
	int triggerVolt = TOP_OF_SCOPEFACE + (triggerVoltLocation * 64);


	while (!((triggerVolt > currentBus) && (triggerVolt < nextBus))) { // While not the intersection
		currentBus = nextBus; 
		nextBus = triggeredBus[currentBusIndex];
		currentBusIndex++;
		if (currentBusIndex > 1023) {
			// Fallback if it goes over the length of the array
			break;
		}
	}

...

```
By swapping the greater than and less than signs, we will look for the rising edge. 

### Hardware schematic

Figure 5 shows how we connect the board to an external display, audio input/outup, and a UART input.  

![Hardware Connection](images/hardware_connections.png)
##### Figure 5: Hardware Connections

### Debugging

During gate check one, I was having difficulty understanding how the two programs were communicating and thus had issues setting up the project. After making my output signals external in my Lab 3 diagram, I was able to fix the issues I was having. 
During gate check two, I had some issues with the way my slave registers was set up and this caused issues and would not update the trigger locations. After fixing the slave registers to communicate to the correct signals, I was able to have Gate Check two functionality complete. 
After the gate checks, there were a few issues with reading the data to be displayed. The first issue was my ready signal. My ISR was not running correctly, but after tracing my ready signal back im my `.vhdl` files, I noticed I was not assigning the proper singal to ready. Another issue I had was there were breaks in my wave while displaying. This issue came from the fact that I was displaying while the `storedLBus` and `storedRBus` were being updated. I fixed this issue by not displaying the waves until the stored busses were completely filled. 

### Testing methodology or results

While using the SDK, testing became much easier. I could use print statements to help ensure the program was running as intended. This was a much quicker and more intuitive form of testing compared to running test benches. I could also ensure the base level functionality was still working by just programming the bitstream and not running the GDB Debugger.

[This Results Video](https://app.vidgrid.com/view/w00jRtfduoHP) shows the full functionality of Lab 3. 

### Answers to Lab Questions

There are no lab questions for lab 3.

### Observations and Conclusions
During this lab, I was able to connect the functionality of lab 2 to an external UART. This allows for a much wider range of inputs thus more allowed functionality. I also created an interupt singal from the `ready` output from the audio codec wrapper. This means the L and R bus storage would be updated every time the audio codec wrapper outputted a value. This lab helped me understand how the board stores information and is able to communicate with an external controller.

### Documentation
The architecture was given in the lab writeup. I used the lec_20.c file as a base file for creating lab_3.c
	/*--------------------------------------------------------------------
	Name: Andres Ruiz
	Date: 3/19/2021
	Course:	CSCE436
	File: lab3.c
	HW:	Lab 3

	Purp:A brief description of what this program does and
		the general solution strategy.

	Academic Integrity Statement: I certify that, while others may have
	assisted me in brain storming, debugging and validating this program,
	the program itself is my own work. I understand that submitting code
	which is the work of other individuals is a violation of the honor
	code.  I also understand that if I knowingly give my original work to
	another individual is also a violation of the honor code.
	-------------------------------------------------------------------------*/

/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"
#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out8 and its variations
#include <xil_exception.h>

/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for the oscope PCORE
 */
#define oscopeBase						0x44a00000
#define oscopeExWriteAddressReg			oscopeBase			// (10) LSB to store a write address
#define	oscopeExWriteEnableReg			oscopeBase+0x4		// (1)  LSB to store write enable
#define oscopeExSelectReg				oscopeBase+0x8		// (1)  LSB to store external select
#define	oscopeLBusOutReg				oscopeBase+0xc		// (16) LSB to store R Bus Out
#define oscopeRBusOutReg				oscopeBase+0x10		// (16) LSB to store L Bus Out
#define	oscopeExternalLBusReg			oscopeBase+0x14		// (16) LSB to store external L Bus
#define oscopeExternalRBusReg			oscopeBase+0x18		// (16) LSB to store external R Bus
#define	oscopeFlagClearReg				oscopeBase+0x1c		// (8)  LSB to store the clear flag value
#define oscopeFlagQReg					oscopeBase+0x20		// (8)  LSB to store flagQ value
#define	oscopeTriggerVoltReg			oscopeBase+0x24		// (10) LSB to store trigger volt value
#define oscopeTriggerTimeReg			oscopeBase+0x28		// (10) LSB to store trigger time value
#define oscopeChannel1EnableReg			oscopeBase+0x2c		// (1)  LSB to store channel one toggle
#define oscopeChannel2EnableReg			oscopeBase+0x30		// (1)  LSB to store channel two toggle

/*
 * The following constants define the oscope commands
 */
#define oscope_WRITE_ENABLED			0x01	// External write enabled
#define	oscope_WRITE_DISABLED			0x00	// External write disabled
#define	oscope_SELECT_ENABLED			0x01	// External select enabled
#define oscope_SELECT_DISABLED			0x00	// External select disabled

#define oscope_FALLING_EDGE_TRIGGER 	0x00	// Trigger off the falling edge
#define oscope_RISING_EDGE_TRIGGER 		0x01 	// Trigger off the rising edge

#define oscope_DEFAULT_TRIGGER_VOLT		0xDC	// Default trigger volt value (220)
#define oscope_DEFAULT_TRIGGER_TIME 	0x140	// Default trigger time value (320)

#define oscope_FULL_CLEAR				0xFF	// Full clear for 8 bit flag register

#define oscope_CHANNEL_ONE				0x01	// Trigger off channel one edge
#define oscope_CHANNEL_TWO				0x00	// Trigger off channel two edge

#define oscope_CHANNEL_ENABLE			0x01	// Display channel enabled
#define oscope_CHANNEL_DISABLE			0x00	// Display channel disabled

#define TOP_OF_SCOPEFACE 					18688

#define printf xil_printf					/* A smaller footprint printf */
#define	uartRegAddr			0x40600000		// read <= RX, write => TX


/************************** Function Prototypes ****************************/
void myISR(void);
void generateSampleWave(void);
void updateWave(void);
void displaySampleWave(void);
void findInitialPrintPointRising(void);
void findInitialPrintPointFalling(void);
void updateBRAM(int);
/************************** Variable Definitions **************************/
/*
 * The following are declared globally so they are zeroed and so they are
 * easily accessible from a debugger
 */
int sampleWave[1024]; // A stored sample wave that is able to be displayed

u16 busAddress = 20; // The current address the bus is written to in the stored L and R bus

int storedLBus[1024];	// The stored L bus from the audio input
int storedRBus[1024];	// The stored R bus from the audio input

int triggerVoltLocation = oscope_DEFAULT_TRIGGER_VOLT; // stores current trigger volt value
int triggerTimeLocation = oscope_DEFAULT_TRIGGER_TIME; // stores current trigger time value

int triggeredChannel = 1; // Stores what channel to trigger off of

int edgeTrigger = 0; // Stores what edge to trigger off of

int displayOnFilledBuffer = 1; // Toggle for the display to update when stored L and R bus are full

int main(void) {

	unsigned char c; 	// Most recent key press

	Xil_Out16(oscopeTriggerVoltReg, triggerVoltLocation);	// Display the trigger volt at the correct location
	Xil_Out16(oscopeTriggerTimeReg, triggerTimeLocation);	// Display the trigger time at the correct location

	Xil_Out8(oscopeChannel1EnableReg, oscope_CHANNEL_ENABLE);	// Turn on channel 1
	Xil_Out8(oscopeChannel2EnableReg, oscope_CHANNEL_ENABLE);	// Turn on channel 2

	init_platform();

	print("Welcome to Lab 3\n\r");
	print("Oscilloscope controlled through UART\n\r");
	print("By: Andres Ruiz\n\r");

    microblaze_register_handler((XInterruptHandler) myISR, (void *) 0);
    microblaze_enable_interrupts();

    Xil_Out8(oscopeFlagClearReg, oscope_FULL_CLEAR);					// Clear the flag and then you MUST
	Xil_Out8(oscopeFlagClearReg, 0x00);									// allow the flag to be reset later

	generateSampleWave();	// Runs function to fill the sample wave memory

    while(1) {

    	c=XUartLite_RecvByte(uartRegAddr);

		switch(c) {

    		/*-------------------------------------------------
    		 * Reply with the help menu
    		 *-------------------------------------------------
			 */
    		case '?':
    			printf("--------------------------\r\n");
    			printf("	Trigger Time: %x\r\n",triggerTimeLocation);
    			printf("	Trigger Volt: %x\r\n",triggerVoltLocation);
				printf("	Flag Register: %x\r\n",Xil_In16(oscopeFlagQReg));
				printf("	Left Bus: %x\r\n",Xil_In16(oscopeLBusOutReg));
				printf("	Right Bus: %x\r\n",Xil_In16(oscopeRBusOutReg));
				printf("--------------------------\r\n");
				printf("	?:   help menu(Done)\r\n");
				printf("	c:   Clear Terminal Window\r\n");
				printf("	p:   Write Sample Wave\r\n");
				printf("	g:   Grab Data to Fill Buffer\r\n");
				printf("	e:   Falling/Rising Edge Trigger\r\n");
				printf("	1/2: Channel 1/2 Trigger\r\n");
				printf("	d:   Increase Trigger Time\r\n");
				printf("	a:   Decrease Trigger Time\r\n");
				printf("	w:   Increase Trigger Voltage\r\n");
				printf("	s:   Decrease Trigger Voltage\r\n");
				printf("	j:   Channel 1 Enable/Disable\r\n");
				printf("	k:   Channel 2 Enable/Disable\r\n");
				printf("	i:   Lab 2 Internal Control\r\n");
    			break;
			/*-------------------------------------------------
			 * Clear the screen
			 *-------------------------------------------------
			 */
    		case 'c':
    			for (c=0; c<40; c++) printf("\r\n");
    			break;
			/*-------------------------------------------------
			 * Display the stored sample wave
			 *-------------------------------------------------
			 */
    		case 'p':
    			displaySampleWave();
    			break;
			/*-------------------------------------------------
			 * Update scopeface to current stored busses
			 *-------------------------------------------------
			 */
    		case 'g':
    			// updateWave();
    			displayOnFilledBuffer = 1;
    			break;
			/*-------------------------------------------------
			 * Trigger off the rising or falling edge
			 *-------------------------------------------------
			 */
    		case 'e':
    			if (edgeTrigger) {
    				edgeTrigger = oscope_FALLING_EDGE_TRIGGER;
    			} else {
    				edgeTrigger = oscope_RISING_EDGE_TRIGGER;
    			}
    			// updateWave();
    			displayOnFilledBuffer = 1;
    			break;
			/*-------------------------------------------------
			 * Trigger off channel 1
			 *-------------------------------------------------
			 */
    		case '1':
    			triggeredChannel = oscope_CHANNEL_ONE;
    			// updateWave();
    			displayOnFilledBuffer = 1;
    			break;
			/*-------------------------------------------------
			 * Trigger off channel 2
			 *-------------------------------------------------
			 */
    		case '2':
    			triggeredChannel = oscope_CHANNEL_TWO;
    			// updateWave();
    			displayOnFilledBuffer = 1;
    			break;
			/*-------------------------------------------------
			 * Move trigger volt up
			 *-------------------------------------------------
			 */
    		case 'w':
    			triggerVoltLocation = triggerVoltLocation - 1;
    			Xil_Out16(oscopeTriggerVoltReg, triggerVoltLocation);
    			// updateWave();
    			displayOnFilledBuffer = 1;
				break;
			/*-------------------------------------------------
			 * Move trigger volt down
			 *-------------------------------------------------
			 */
        	case 's':
        		triggerVoltLocation = triggerVoltLocation + 1;
        		Xil_Out16(oscopeTriggerVoltReg, triggerVoltLocation);
        		// updateWave();
        		displayOnFilledBuffer = 1;
        		break;
			/*-------------------------------------------------
			 * Move trigger time left
			 *-------------------------------------------------
			 */
        	case 'a':
        		triggerTimeLocation = triggerTimeLocation - 1;
        		Xil_Out16(oscopeTriggerTimeReg, triggerTimeLocation);
        		// updateWave();
        		displayOnFilledBuffer = 1;
				break;
			/*-------------------------------------------------
			 * Move trigger time right
			 *-------------------------------------------------
			 */
			case 'd':
				triggerTimeLocation = triggerTimeLocation + 1;
				Xil_Out16(oscopeTriggerTimeReg, triggerTimeLocation);
				// updateWave();
				displayOnFilledBuffer = 1;
				break;
			/*-------------------------------------------------
			 * Toggle channel 1 enable / disable
			 *-------------------------------------------------
			 */
			case 'j':
				if (Xil_In8(oscopeChannel1EnableReg)) {
					Xil_Out8(oscopeChannel1EnableReg, oscope_CHANNEL_DISABLE);
				} else {
					Xil_Out8(oscopeChannel1EnableReg, oscope_CHANNEL_ENABLE);
				}
				break;
			/*-------------------------------------------------
			 * Toggle channel 2 enable / disable
			 *-------------------------------------------------
			 */
			case 'k':
				if (Xil_In8(oscopeChannel2EnableReg)) {
					Xil_Out8(oscopeChannel2EnableReg, oscope_CHANNEL_DISABLE);
				} else {
					Xil_Out8(oscopeChannel2EnableReg, oscope_CHANNEL_ENABLE);
				}
				break;
			/*-------------------------------------------------
			 * Lab 2 vs Lab 3 functionality
			 *-------------------------------------------------
			 */
			case 'i':
				if (Xil_In8(oscopeExSelectReg)) {
					Xil_Out8(oscopeExSelectReg, oscope_SELECT_DISABLED);
				} else {
					Xil_Out8(oscopeExSelectReg, oscope_SELECT_ENABLED);
				}
				break;
    		default:
    			printf("unrecognized character: %c\r\n",c);
    			break;
    	} // end case

    } // end while 1

    cleanup_platform();

    return 0;
} // end main


void myISR(void) {

	// Store L and R bus off every ready signal
	storedLBus[busAddress] = Xil_In16(oscopeLBusOutReg);
	storedRBus[busAddress] = Xil_In16(oscopeRBusOutReg);

	busAddress++;						// Incrementing the external write address

	if (busAddress > 1023) { // If at the max count then reset
		busAddress = 20;
		// If waiting on display, update display and reset display variable
		if (displayOnFilledBuffer) {
			updateWave();
			displayOnFilledBuffer = 0;
		}
	}


	Xil_Out8(oscopeFlagClearReg, 0x01);					// Clear the flag and then you MUST
	Xil_Out8(oscopeFlagClearReg, 0x00);					// allow the flag to be reset later

}

void generateSampleWave(void) {
	sampleWave[19] = 30848; // Have a starting value
	int change = 64;		// How much the 16 bit number changes off each trigger volt press
	// Fill array based off current location in display
	for (int i = 20; i <= 620; i++) {
		if (i % 200 < 100 ) {
			sampleWave[i] = sampleWave[i-1] + change;
		} else {
			sampleWave[i] = sampleWave[i-1] - change;
		}
	}
}

void displaySampleWave(void) {
	// Update display with sample wave values
	for (int i = 0; i <= 1023; i++) {
		Xil_Out16(oscopeExWriteAddressReg, i-120);
		Xil_Out16(oscopeExternalLBusReg, sampleWave[i]);
		Xil_Out16(oscopeExternalRBusReg, sampleWave[i]);
		Xil_Out8(oscopeExWriteEnableReg, 0x01);
		Xil_Out8(oscopeExWriteEnableReg, 0x00);
	}
}

void updateWave(void) {
	// Update wave based off which edge trigger
	if (edgeTrigger) {
		findInitialPrintPointRising();
	} else {
		findInitialPrintPointFalling();
	}
}

void findInitialPrintPointRising(void) {

	int triggeredBus[1024];
	// Select which bus to use
	if (triggeredChannel) {
		for (int i = 0; i <= 1023; i++) {
			triggeredBus[i] = storedLBus[i];
		}
	} else {
		for (int i = 0; i <= 1023; i++) {
			triggeredBus[i] = storedRBus[i];
		}
	}

	int printingLocation;
	int currentBusIndex = triggerTimeLocation + 2; // Counter to keep track of bus location
	int nextBus = triggeredBus[triggerTimeLocation + 1]; // Bus location following previous bus
	int currentBus = triggeredBus[triggerTimeLocation]; // First bus location
	int triggerVolt = TOP_OF_SCOPEFACE + (triggerVoltLocation * 64); // Find the correct value for tirggerVolt based off BRAM values

	// Find intersection
	while (!((triggerVolt < currentBus) && (triggerVolt > nextBus))) {
		currentBus = nextBus;
		nextBus = triggeredBus[currentBusIndex];
		currentBusIndex++;
		if (currentBusIndex > 1023) {
			// Fallback if it goes over the length of the array
			break;
		}
	}
	// Find the shifted printing location
	printingLocation = (currentBusIndex - triggerTimeLocation + 20);
	// Update BRAM
	updateBRAM(printingLocation);
}

void findInitialPrintPointFalling(void) {

	int triggeredBus[1024];

	if (triggeredChannel) {
		for (int i = 0; i <= 1023; i++) {
			triggeredBus[i] = storedLBus[i];
		}
	} else {
		for (int i = 0; i <= 1023; i++) {
			triggeredBus[i] = storedRBus[i];
		}
	}

	int printingLocation;
	int currentBusIndex = triggerTimeLocation + 2; // Counter to keep track of bus location
	int nextBus = triggeredBus[triggerTimeLocation + 1]; // Bus location following previous bus
	int currentBus = triggeredBus[triggerTimeLocation]; // First bus location
	int triggerVolt = TOP_OF_SCOPEFACE + (triggerVoltLocation * 64); // Find the correct value for tirggerVolt based off BRAM values

	// Find intersection
	while (!((triggerVolt > currentBus) && (triggerVolt < nextBus))) {
		currentBus = nextBus;
		nextBus = triggeredBus[currentBusIndex];
		currentBusIndex++;
		if (currentBusIndex > 1023) {
			// Fallback if it goes over the length of the array
			break;
		}
	}
	// Find the shifted printing location
	printingLocation = (currentBusIndex - triggerTimeLocation + 20);
	// Update BRAM
	updateBRAM(printingLocation);
}

void updateBRAM(int printingLocation) {
	for (int i = 20; i <= 620; i++) { // For loop over scopeface display
		Xil_Out16(oscopeExWriteAddressReg, i); // Write address at current for loop index
		Xil_Out16(oscopeExternalLBusReg, storedLBus[printingLocation]); // Bus address at the current print location
		Xil_Out16(oscopeExternalRBusReg, storedRBus[printingLocation]);
		Xil_Out8(oscopeExWriteEnableReg, 0x01); // Enable then disable writing
		Xil_Out8(oscopeExWriteEnableReg, 0x00);
		printingLocation++;
	}
}

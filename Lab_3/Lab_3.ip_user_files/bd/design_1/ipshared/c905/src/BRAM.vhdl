--------------------------------------------------------------------
-- Name:	Andres Ruiz
-- Date:	Mar 3, 2021
-- File:	BRAM.vhdl
-- HW:	    Lab 2
-- Crs:	    CSCE 436
--
-- Purp:	A datapath integrating a small RAM in preparation for Lab 2
--
-- Documentation:	I pulled the BRAM example from the Xilinx technical
--						documents.  They were significantly modified to get
--						get them into their current form.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
--
--
------------------------------------------------------------------------- 
library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;				    -- contains the unsigned data type 
library UNIMACRO;							-- This contains links to the Xilinx block RAM
use UNIMACRO.vcomponents.all;

entity Lab2Dual_dp is
	Port(	clk: in  STD_LOGIC;
			n_reset : in  STD_LOGIC;
			WRADDR: in unsigned(9 downto 0);
			Din: in std_logic_vector (17 downto 0);
			WREN: in std_logic;
			rENB : in STD_LOGIC;
			RDADDR : in unsigned(9 downto 0); 
			DOUT: out std_logic_vector(17 downto 0));
end Lab2Dual_dp;

architecture behavior of Lab2Dual_dp is
    signal readOutput: std_logic_vector(17 downto 0);
	signal reset: std_logic;
	signal readAddress, writeAddress: std_logic_vector(9 downto 0);
    
    signal Din_vector: std_logic_vector (17 downto 0);
begin		
	----------------------------------------------------------------------------	
	-- Reference:	Vivado Design Suite 7 Series FPGA Libraries Guide 
	--              UG953 (v 2012.4) July 25, 2012
	--              
	-- Page:			10
	-----------------------------------------------------------------------------
	reset <= not n_reset;
	-- Changing to the correct data type
	-- readAddress <= std_logic_vector(RDADDR);
	writeAddress <= std_logic_vector(WRADDR);
	readAddress <= std_logic_vector(RDADDR);
	
	sampleMemory: BRAM_SDP_MACRO
		generic map (
			BRAM_SIZE => "18Kb", 					-- Target BRAM, "18Kb" or "36Kb"
			DEVICE => "7SERIES", 					-- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
			DO_REG => 0, 							-- Optional output register disabled
			INIT => X"000000000000000000",			-- Initial values on output port
			INIT_FILE => "NONE",					-- Not sure how to initialize the RAM from a file
			WRITE_WIDTH => 18, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			READ_WIDTH => 18, 						-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			SIM_COLLISION_CHECK => "NONE", 			-- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
			SRVAL => X"000000000000000000")			-- Set/Reset value for port output
		port map (
			DO => readOutput,						-- Output read data port, width defined by READ_WIDTH parameter
			RDADDR => readAddress,					-- Input address, width defined by port depth
			RDCLK => clk,	 						-- 1-bit input clock
			RST => reset,							-- active high reset
			RDEN => rENB,							-- read enable 
			REGCE => '1',							-- 1-bit input read output register enable - ignored
			DI => Din,						        -- Input data port, width defined by WRITE_WIDTH parameter
			WE => "11",					            -- since RAM is byte read, this determines high or low byte
			WRADDR => writeAddress,					-- Input write address, width defined by write port depth
			WRCLK => clk,							-- 1-bit input write clock
			WREN => WREN);					 		-- 1-bit input write port enable

DOUT <= readOutput;

end behavior;
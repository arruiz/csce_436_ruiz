----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        2/23/2021
-- Course:      CSCE 436
-- File:        lab2_fsm.vhdl
-- HW:          Lab 2
-- Purp:        Control Unit for the lab 2 portion of the oscilloscope
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab2_fsm is
    Port(
    clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	sw: in std_logic_vector(2 downto 0);
	cw: out std_logic_vector (2 downto 0));
end lab2_fsm;

architecture Behavioral of lab2_fsm is
    type state_type is (TriggerWait, ReadyWait, Counting);
    signal state: state_type;
    -- Status Word Bit Representations
    constant MaxCount: integer := 0;
    constant Trigger: integer := 1;
    constant ReadySW: integer := 2;
begin

state_process: process(clk,reset_n)
begin
    if (rising_edge(clk)) then
        if (reset_n = '0') then
            state <= TriggerWait;
        else 
            case state is 
                when TriggerWait =>
                    if (sw(Trigger) = '1') then state <= ReadyWait;
                    else state <= TriggerWait; end if;
                when ReadyWait =>
                    if (sw(ReadySW) = '1') and (sw(MaxCount) = '1') then state <= TriggerWait;
                    elsif (sw(ReadySW) = '1') then state <= Counting;
                    else state <= ReadyWait; end if;
                when Counting =>
                    state <= ReadyWait;
            end case;
        end if;
    end if;
end process;

cw <= "011" when state = TriggerWait else
      "000" when state = ReadyWait else
      "101" when state = Counting else
      "000";

end Behavioral;

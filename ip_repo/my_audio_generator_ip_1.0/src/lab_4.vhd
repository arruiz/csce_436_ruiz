----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        2/23/2021
-- Course:      CSCE 436
-- File:        lab2.vhdl
-- HW:          Lab 2
-- Purp:        Top level entity for lab 2.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.lab4Parts.all;		


entity lab4 is
    Port ( clk : in  STD_LOGIC;
           -- ready : in std_logic;
           reset_n : in  STD_LOGIC;
		   ac_mclk : out STD_LOGIC;
		   ac_adc_sdata : in STD_LOGIC;
		   ac_dac_sdata : out STD_LOGIC;
		   ac_bclk : out STD_LOGIC;
		   ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;
		   btn: in	STD_LOGIC_VECTOR(4 downto 0);
		   switch: in STD_LOGIC_VECTOR(7 downto 0));
end lab4;

 architecture behavior of lab4 is

	signal sw: std_logic_vector(1 downto 0);
	signal cw: std_logic_vector (2 downto 0);

	 
begin
	
	datapath: lab4_datapath port map(
	    -- ready => ready,
		clk => clk,
		reset_n => reset_n,
		ac_mclk => ac_mclk,
		ac_adc_sdata => ac_adc_sdata,
		ac_dac_sdata => ac_dac_sdata,
		ac_bclk => ac_bclk,
		ac_lrclk => ac_lrclk,
        scl => scl,
        sda => sda,
		sw => sw,
		cw => cw,
		btn => btn, 
		switch => switch,
		Lbus_out => OPEN,
		Rbus_out => OPEN);

	  
	control: lab4_fsm port map( 
		clk => clk,
		reset_n => reset_n,
		sw => sw,
		cw => cw);

end behavior;
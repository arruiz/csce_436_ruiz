----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      counter.vhdl
-- HW:        Homework 4
-- Purp:      Top file for running cascaded counters
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------

library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;


entity hw4 is
        port(	clk, reset: in std_logic; 
		ctrl: in std_logic;
		Q1, Q0: out unsigned(2 downto 0));
end hw4;

architecture Behavioral of hw4 is

COMPONENT counter is 
    port(	clk, reset: in std_logic; 
		ctrl: in std_logic;
		Q: out unsigned(2 downto 0);
		roll: out std_logic
		);
end COMPONENT;

   --Input
   signal sCtrl : std_logic := '0';

 	--Outputs
   signal sRoll : std_logic;

begin
     least_significant_counter: counter
         PORT MAP (
              clk => clk,
              reset => reset,
              ctrl => ctrl,
              Q => Q0,
              roll => sRoll
            );
     most_significant_counter: counter
         PORT MAP (
              clk => clk,
              reset => reset,
              ctrl => sCtrl,
              Q => Q1
            );
      
process(clk)
begin

    sCtrl <= sRoll and ctrl;
    
end process;

end Behavioral;

----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      counter.vhdl
-- HW:        Homework 4
-- Purp:      Create a mod 5 counter
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;


entity counter is
        port(	clk, reset: in std_logic; 
		ctrl: in std_logic;
		Q: out unsigned(2 downto 0);
		roll: out std_logic);
end counter;

architecture behavior of counter is
	signal processQ: unsigned (2 downto 0);

begin
	
	
	-----------------------------------------------------------------------------
	--		ctrl
	--		00			hold
	--		01			count up mod 10
	--		10			load D
	--		11			synch reset
	-----------------------------------------------------------------------------
	process(clk)
	begin
	    -----------------------------------------------------------------------------
        --		ctrl
        --		0			hold
        --		1			count up mod 10
        -----------------------------------------------------------------------------
	    -- Least significant
		if (rising_edge(clk)) then
			if (reset = '0') then
				processQ <= (others => '0');
			elsif ((processQ < 4) and (ctrl = '1')) then
				processQ <= processQ + 1;
			elsif ((processQ = 4) and (ctrl = '1')) then
				processQ <= (others => '0');
			end if;
		end if;
		
		-- Most significant
--		if (rising_edge(clk)) then
--			if (reset = '0') then
--				processQ1 <= (others => '0');
--				rollSynch <= '0';
--			elsif ((processQ1 = 4) and (ctrl = '1') and rollCombo = '1') then
--				processQ1 <= (others => '0');
--			elsif ((rollCombo = '1') and (ctrl = '1')) then
--				processQ1 <= processQ1 + 1;
--			end if;
--		end if;
	end process;
 
	-- CSA
	roll  <= '1' when (processQ = 4) else '0';
	Q <= processQ;
	
end behavior;
----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      counter_tb.vhdl
-- HW:        Homework 4
-- Purp:      Test bench for counter.vhdl
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY hw4_tb IS
END hw4_tb;
 
ARCHITECTURE behavior OF hw4_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT hw4
        port(	clk, reset: in std_logic; 
		ctrl: in std_logic;
		Q1, Q0: out unsigned(2 downto 0));
    end COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal ctrl : std_logic := '0';

 	--Outputs
   signal Q0,Q1 : unsigned(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 500 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: hw4 PORT MAP (
          clk => clk,
          reset => reset,
		  ctrl => ctrl,
          Q0 => Q0,
          Q1 => Q1
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
 	-----------------------------------------------------------------------------
	--		ctrl
	--		0			hold
	--		1			count up mod 10
	-----------------------------------------------------------------------------
	ctrl <= '1', '0' after 8us, '1' after 9us, '0' after 19us;
	reset <= '0', '1' after 1us, '0' after 16us, '1' after 17us;

END;

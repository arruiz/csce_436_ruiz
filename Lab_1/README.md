# Lab 1 - Video Synchronization

## By Andres Ruiz
## Table of Contents (not required but makes things easy to read)
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose 
The goal of this lab was to create the video synchronization of the oscilloscope we will be creating throughout this lab. So I will be creating counters for the synchronization signals for both the horizontal and vertical pixel drawings. I will also be creating a scopeface for the VGA to determine which colors to display at the corresponding pixels. The final file to create will be the VGA. This will link the counters and scopeface to allow a display of the grid, channel waves, and trigger markers. The final step is to link the buttons to the trigger volt and trigger time values and have them move with the corresponding button.

### Preliminary design
During homework 5, we created a drawing of the scopeface. Figure 1 shows the drawing and the relating pixel values for each portion of the drawing.

![Homework 5 Drawing](Images/drawing.jpg)
##### Figure 1: Homework 5 display drawing

Following the architecture  provided in the Lab, I noticed that I would have to start with two cascading counters for the row and column values of the display. Using the counter created in the Homework 4 assignment, I could modify the mod values so they reach the appropriate values. Figure 2 shows the test bench for the cascading counters.

![Signal](Images/gate_check_1_waveform.png)
##### Figure 2: Waveform for the column and row cascading counters

The next step was to create a working display. To do this, we must give the appropriate values to the h_sync, v_sync, and blank signals. Following the column in row values, we are able to give the appropriate values to those signals. Figure 3, 4, 5, and 6 show the corresponding  signals and their relationships.

![Signal](Images/h_sync_high.png)
##### Figure 3: h_sync going high

![Signal](Images/h_sync_low.png)
##### Figure 4: h_sync going low

![Signal](Images/blank_h_relation.png)
##### Figure 5: blank signal in relation to h_sync

![Signal](Images/blank_v_relation.png)
##### Figure 6: blank signal in relation to v_sync

#### Code:
 
##### Debouncing buttons
```
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      lab1.vhdl
-- Lab:       Lab1
-- Purp:      top-level entity for lab 1
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
...
process(clk)
begin
    if (rising_edge(clk)) then
        if (reset_n = '0') then
			-- reset when reset toggle was active
            button_activity <= "00000";
        else 
			-- otherwise set button activity
            button_activity <= (old_button XOR btn) and btn;
        end if;
	-- replace old button with current button for next debounce cycle
    old_button <= btn;
    end if;
end process;
        
process(clk)
begin        
    if (rising_edge(clk)) then
        if (reset_n = '0') then
            trigger_time <= to_unsigned(320,10);
            trigger_volt <= to_unsigned(220,10);
        elsif (button_activity(0) = '1') then -- move up trigger volt when button pressed
            trigger_volt <= trigger_volt - 1;
        elsif (button_activity(1) = '1')      -- move left trigger time when button pressed
            trigger_time <= trigger_time - 1;
        elsif (button_activity(2) = '1') then -- move down trigger volt when button pressed
            trigger_volt <= trigger_volt + 1;
        elsif (button_activity(3) = '1') then -- move right trigger time when button pressed
            trigger_time <= trigger_time + 1;
        else 
            trigger_time <= trigger_time;	  -- otherwise dont change the values
			trigger_volt <= trigger_volt;
        end if;
    end if;
end process;
...
```
The code above was used to debounce the buttons. Without a debounce, pressing the buttons would cause the trigger volt values to move too fast for the display to keep up. This was the main source of my problems during the lab. 

##### Glue logic for VGA file
```
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      vga.vhdl
-- Lab:       Lab1
-- Purp:      vga file for lab 1
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
...
    -- glue logic for cascading counter
    row_ctrl <= col_roll and ctrl;
    
    -- Calculating h_sync value
    if (col_number < 656 or col_number >= 752) then
        h_sync <= '1';
    else 
        h_sync <= '0';
    end if;
    
    -- Calculating v_sync value
    if (row_number < 490 or row_number >=492) then
        v_sync <= '1';
    else 
        v_sync <= '0';
    end if;
    
    -- Calculating blank value
    if (col_number >= 640 or row_number >=480) then
        blank <= '1';
    else 
        blank <= '0';
    end if;
...
```

The code snippet above contains the glue logic for the VGA. This calculates the h_sync, v_sync, and blank values to be used in the scope face. It also contains the glue logic for the column counter roll. 

### Software flow chart or algorithms

#### Pseudocode:
Pseudocode for drawing scopeface grid:  
```
	IF (column mod 60) = 20 THEN  
	    white = '1'  
	ELSE IF (row mod 50) = 20 THEN  
		white = '1'  
	ELSE IF (row = 219) and (column mod 15 = 5) THEN  
		white = '1'   
	ELSE IF (row = 221) and (column mod 15 = 5) THEN  
		white = '1'   
	ELSE IF (column = 319) and (row mod 10 = 0) THEN  
		white = '1'  
	ELSE IF (column = 321) and (row mod 10 = 0) THEN  
		white = '1'  
	ELSE  
		white = '0'  
```

#### Architecture

This is the architecture provided in the lab writeup that was used in the creation of this project.

![Signal](Images/Lab_1_Architecture.png)
##### Figure 7: Architecture used in Lab 1

#### VGA Protocol

Using the counters, we can determine where in the VGA protocol we are. The counters help determine the activity of the h_sync, v_sync, and blank signals which help with the display as shown in Figure 8. 

![Signal](Images/signals.jpg)
##### Figure 8: VGA Protocol for scopeface

### Debugging

One major issue I had was debouncing the button. The at first I was not sure why the trigger volt line was disappearing after pressing the up or down button. After adding a higher priority to the trigger volt line, I realized it was jumping to the top and bottom of the grid. After looking into the test bench, I noticed the trigger volt value was increasing while the button was pressed for every clock cycle. So, all I needed to do was debounce the button. However, after debouncing the button, I was no longer able to move the trigger volt line. While looking into my testbench, I noticed that the value was increasing as it was supposed to be, however, the display was not working. I am yet to figure out the cause of this issue.

Update: After speaking with Professor Falkinburg, I was able to get the debounce to work. 

While drawing the grid and channel waves, I ran into an issue to determine which colors should have a higher priority and which lines have a higher priority. Some I had some ideas to assign black with the highest priority and draw them over at the beginning or at the end. However, it made the code a bit confusing to read. In the end, I decided it would be better to be more specific with where the lines would be drawn by adding boundaries to each of the column and row lines (ie. the row would only be drawn if it was between columns 20 and 620). Although this added repetition in my code, the readability is more important in this instance.

### Testing methodology or results

During gate check one, I used the given testbench to make sure our counters were working. During gate check two, I used the bitstream to test. This allowed me to see if the code was actually  displaying. While it did take a couple of attempts, I was eventually able to display the file. For the functionality, I realized a test bench would be much easier and time effective for testing. I created a test bench to see the values of trigger volt and trigger time. This allowed for much quicker testing and results.

[Link to final results video](https://www.youtube.com/watch?v=gJyvgfeXJ8Q)

### Answers to Lab Questions
No questions are given in the lab writeup

### Observations and Conclusions
During this lab, learned a lot about creating and using a testbench in VHDL. Although a test bench is another location for errors and buggy code, I realized that creating a working test bench is crucial to the process. It saves time with generating bitstreams, shows the values that are hidden while the program runs, and allows for a better understanding of what the code is doing under the hood. 

During this lab, I was able to create a working display with the grid and markers. However, I was not able to get functioning buttons to update the trigger volt and trigger time values. I do plan to continue debugging to find the cause of this. 

Update: I was able to add button functionality to the display.

### Documentation
The architecture was given in the lab writeup, and the debouncing information was given during a meeting with Professor Falkinburg.

----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      vga_tb.vhdl
-- Lab:       Lab1
-- Purp:      test bench for vga file for lab 1
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY lab1_tb IS
END lab1_tb;
 
ARCHITECTURE behavior OF lab1_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT lab1
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
		   btn: in	STD_LOGIC_VECTOR(4 downto 0);
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0));
    END COMPONENT;
    
    
    COMPONENT video
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
			  trigger_time: in unsigned(9 downto 0);
			  trigger_volt: in unsigned (9 downto 0);
			  row: out unsigned(9 downto 0);
			  column: out unsigned(9 downto 0);
			  ch1: in std_logic;
			  ch1_enb: in std_logic;
			  ch2: in std_logic;
			  ch2_enb: in std_logic);
    END COMPONENT;
    
    COMPONENT vga
	Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			h_sync : out  STD_LOGIC;
			v_sync : out  STD_LOGIC; 
			blank : out  STD_LOGIC;
			r: out STD_LOGIC_VECTOR(7 downto 0);
			g: out STD_LOGIC_VECTOR(7 downto 0);
			b: out STD_LOGIC_VECTOR(7 downto 0);
			trigger_time: in unsigned(9 downto 0);
			trigger_volt: in unsigned (9 downto 0);
			row: out unsigned(9 downto 0);
			column: out unsigned(9 downto 0);
			ch1: in std_logic;
			ch1_enb: in std_logic;
			ch2: in std_logic;
			ch2_enb: in std_logic);
    END COMPONENT;
    
    COMPONENT col_counter
      port(	clk, reset_n: in std_logic; 
		ctrl: in std_logic;
		col_number: out unsigned(9 downto 0);
		col_roll: out std_logic);
    END COMPONENT;
    
    COMPONENT row_counter
      port(	clk, reset_n: in std_logic; 
		ctrl: in std_logic;
		row_number: out unsigned(9 downto 0);
		row_roll: out std_logic);
	END COMPONENT;
	
    COMPONENT scopeFace
        Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
		   trigger_volt: in unsigned (9 downto 0);
		   trigger_time: in unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
           ch1: in std_logic;
           ch1_enb: in std_logic;
           ch2: in std_logic;
           ch2_enb: in std_logic);
    END COMPONENT;
    
       --Inputs
    signal trigger_time_signal: unsigned (9 downto 0);
    signal trigger_volt_signal: unsigned (9 downto 0);
	signal row, column: unsigned(9 downto 0);
	signal old_button, button_activity: std_logic_vector(4 downto 0) := "00000";
	signal ch1_wave, ch2_wave: std_logic;
	signal ctrl: std_logic;    
       
   signal btn: STD_LOGIC_VECTOR(4 downto 0);
   signal trigger_time: unsigned (9 downto 0);
   signal trigger_volt: unsigned (9 downto 0);
   signal clk : std_logic;
   signal reset_n : std_logic;
   signal row_signal : unsigned(9 downto 0) := (others => '0');
   signal column_signal : unsigned(9 downto 0) := (others => '0');
   signal ch1, ch1_enb, ch2, ch2_enb: std_logic := '0';
	
 	--Outputs
   signal h_sync : std_logic;
   signal v_sync : std_logic;
   signal blank : std_logic;

   signal r : std_logic_vector(7 downto 0);
   signal g : std_logic_vector(7 downto 0);
   signal b : std_logic_vector(7 downto 0);
   signal tmds: std_logic_vector(3 downto 0);
   signal tmdsb: std_logic_vector(3 downto 0);
   -- Clock period definitions
   constant clk_period : time := 40 ns; --changed clock period to generate a 25 MHZ Signal 
 
BEGIN
 
	-- Instantiate the VGA Unit Under Test (UUT)
   uut_lab1: lab1 PORT MAP (
           clk => clk,
           reset_n  => reset_n,
		   btn  => btn,
           tmds => tmds,
           tmdsb => tmdsb
         );   
         
    uut_video: video PORT MAP (
           clk => clk,
           reset_n  => reset_n,
           tmds => tmds,
           tmdsb => tmdsb,
		   trigger_volt => trigger_time_signal,
           trigger_time => trigger_time_signal,
           row => row_signal,
           column => column_signal,
           ch1 => ch1,
           ch1_enb => ch1_enb,
           ch2 => ch2,
           ch2_enb => ch2_enb
        );    
         
    uut_VGA: vga PORT MAP (
          clk => clk,
          reset_n => reset_n,
          h_sync => h_sync,
          v_sync => v_sync,
          blank => blank,
          r => r,
          g => g,
          b => b,
          trigger_volt => trigger_time_signal,
          trigger_time => trigger_time_signal,
          row => row_signal,
          column => column_signal,
          ch1 => ch1,
          ch1_enb => ch1_enb,
          ch2 => ch2,
          ch2_enb => ch2_enb			 
        );
	
    uut_scopeFace: scopeFace PORT MAP ( 
           row => row_signal,
           column => column_signal,
		   trigger_volt => trigger_time_signal,
		   trigger_time => trigger_time_signal,
           r => r,
           g => g,
           b => b,
           ch1 => ch1,
           ch1_enb => ch1_enb,
           ch2 => ch2,
           ch2_enb => ch2_enb
           );
   
      -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
   reset_n <= '0', '1' after 30nS;
   btn <= "00000", 
          "00100" after 120ns, "00000" after 200ns,
          "00100" after 280ns, "00000" after 360ns,
          "00100" after 440ns;

END;
----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      counter_tb.vhdl
-- HW:        Homework 4
-- Purp:      Test bench for counter.vhdl
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY counter_tb IS
END counter_tb;
 
  ARCHITECTURE behavior OF counter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    
    COMPONENT col_counter
        port(	clk: in std_logic;
        reset_n: in std_logic; 
		ctrl: in std_logic;
		col_number: out unsigned(9 downto 0);
		col_roll: out std_logic);
    end COMPONENT;
    
    COMPONENT row_counter
        port(	clk, reset_n: in std_logic; 
		ctrl: in std_logic;
	    row_number: out unsigned(9 downto 0);
		row_roll: out std_logic);
    end COMPONENT;

   --Signal Declarations for the UUT
   --Inputs
   signal clk : std_logic := '0';
   signal reset_n : std_logic := '0';
   signal ctrl : std_logic := '1';

 	--Outputs

    signal col_number : unsigned(9 downto 0);
    signal row_number : unsigned(9 downto 0);
    signal col_roll: std_logic;
    signal row_roll: std_logic;
    signal row_ctrl: std_logic;
   -- Clock period definitions
   constant clk_period : time := 40 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut_col_counter: col_counter PORT MAP (
          clk => clk,
          reset_n => reset_n,
		  ctrl => ctrl,
          col_number => col_number,
          col_roll => col_roll
        );
        
   uut_row_counter: row_counter PORT MAP (
          clk => clk,
          reset_n => reset_n,
		  ctrl => row_ctrl,
          row_number => row_number,
          row_roll => row_roll
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
 	-----------------------------------------------------------------------------
	--		ctrl
	--		0			hold
	--		1			count up mod 10
	-----------------------------------------------------------------------------
    reset_n <= '0', '1' after 80ns;
    row_ctrl <= ctrl and col_roll;
END;

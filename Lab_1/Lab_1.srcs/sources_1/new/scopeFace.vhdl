----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      scopeFace.vhdl
-- Lab:       Lab1
-- Purp:      scopeFace file for lab 1
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity scopeFace is
    Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
		   trigger_volt: in unsigned (9 downto 0);
		   trigger_time: in unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
           ch1: in std_logic;
           ch1_enb: in std_logic;
           ch2: in std_logic;
           ch2_enb: in std_logic);
end scopeFace;
architecture Behavioral of scopeFace is
    signal adjusted_column: unsigned(9 downto 0) := (column - 20);
    signal adjusted_row: unsigned(9 downto 0) := (row - 20);
    signal white: std_logic := '0';
    signal green: std_logic := '0';
    signal blue: std_logic := '0';
    signal yellow: std_logic := '0';
begin 
                         
    yellow <= '1' when ((ch1 = '1') and (row >= 20 and row <= 420)) else
              '0';
              
    green <= '1' when ((ch2 = '1') and (row >= 20 and row <= 420) and (column >= 20 and column <= 620)) else
              '0';
              
    blue <= '1' when ((row = trigger_volt) and (column >= 20 and column <= 620)) else
            '0';
    
    white <= '1' when ((((row mod 50) = 20) and (column >= 20 and column <= 620 and row <=420)) or -- columns
                       (((column mod 60) = 20) and (row >= 20 and row <= 420)) or -- rows
                        ((row = 219) and ((adjusted_column mod 15) = 0) and (column >= 20 and column <= 620)) or -- row hashes
                        ((row = 221) and ((adjusted_column mod 15) = 0) and (column >= 20 and column <= 620)) or -- row hashes
                        ((column = 319) and ((adjusted_row mod 10) = 0) and (row >= 20 and row <= 420)) or -- column hashes
                        ((column = 321) and ((adjusted_row mod 10) = 0) and (row >= 20 and row <= 420))) else -- column hashes
             '0';             
    
    r <= x"00" when (blue = '1') else
         x"FF" when (white = '1') else
         x"FF" when (yellow = '1') else 
         x"00";
    g <= x"00" when (blue = '1') else
         x"FF" when (white = '1') else 
         x"FF" when (green = '1') else 
         x"FF" when (yellow = '1') else 
         x"00";
    b <= x"FF" when (white = '1') else 
         x"FF" when (blue = '1') else
         x"00";
         
end Behavioral;
----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      lab1.vhdl
-- Lab:       Lab1
-- Purp:      top-level entity for lab 1
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity lab1 is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
		   btn: in	STD_LOGIC_VECTOR(4 downto 0);
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0));
end lab1;

architecture structure of lab1 is
    signal trigger_time_signal: unsigned (9 downto 0);
    signal trigger_volt_signal: unsigned (9 downto 0);
	signal row, column: unsigned(9 downto 0);
	signal old_button, button_activity: std_logic_vector(4 downto 0) := "00000";
	signal ch1_wave, ch2_wave: std_logic;
	signal ctrl: std_logic;
	component video is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
		   trigger_time: in unsigned(9 downto 0);
		   trigger_volt: in unsigned (9 downto 0);
           row: out unsigned(9 downto 0);
           column: out unsigned(9 downto 0);
           ch1: in std_logic;
           ch1_enb: in std_logic;
           ch2: in std_logic;
           ch2_enb: in std_logic);
	end component;

begin
	------------------------------------------------------------------------------
	------------------------------------------------------------------------------
ch1_wave <= '1' when (row = column) else
       '0';
       
ch2_wave <= '1' when (row = 440 - column) else
       '0';
	
button_activity(0) <= '1' when (old_button(0) /= btn(0)) else '0';
button_activity(1) <= '1' when (old_button(1) /= btn(1)) else '0';
button_activity(2) <= '1' when (old_button(2) /= btn(2)) else '0';
button_activity(3) <= '1' when (old_button(3) /= btn(3)) else '0';

trigger_volt_signal <= (trigger_volt_signal - 1) when (button_activity(0) = '1' and (trigger_volt_signal > 20)) else
                (trigger_volt_signal + 1) when (button_activity(2) = '1' and (trigger_volt_signal < 420)) else
                ("0011011101") when (reset_n = '0') else
                (trigger_volt_signal);

trigger_time_signal <= (trigger_time_signal - 1) when (button_activity(1) = '1' and (trigger_time_signal < 20)) else
                (trigger_time_signal + 1) when (button_activity(3) = '1' and (trigger_time_signal > 620)) else
                ("0101000001") when (reset_n = '0') else
                (trigger_time_signal);

        video_inst: video port map( 
		clk => clk,
		reset_n => reset_n,
		tmds => tmds,
		tmdsb => tmdsb,
		trigger_time => trigger_time_signal,
		trigger_volt => trigger_volt_signal,
		row => row, 
		column => column,
		ch1 => ch1_wave,
		ch1_enb => ch1_wave,
		ch2 => ch2_wave,
		ch2_enb => ch2_wave); 

old_button <= btn;
end structure;

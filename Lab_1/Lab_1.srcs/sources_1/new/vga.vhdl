----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:      Andres Ruiz
-- Date:      2/2/2021
-- Course:    CSCE 436
-- File:      vga.vhdl
-- Lab:       Lab1
-- Purp:      vga file for lab 1
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vga is
	Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			h_sync : out  STD_LOGIC;
			v_sync : out  STD_LOGIC; 
			blank : out  STD_LOGIC;
			r: out STD_LOGIC_VECTOR(7 downto 0);
			g: out STD_LOGIC_VECTOR(7 downto 0);
			b: out STD_LOGIC_VECTOR(7 downto 0);
			trigger_time: in unsigned(9 downto 0);
			trigger_volt: in unsigned (9 downto 0);
			row: out unsigned(9 downto 0);
			column: out unsigned(9 downto 0);
			ch1: in std_logic;
			ch1_enb: in std_logic;
			ch2: in std_logic;
			ch2_enb: in std_logic);
end vga;

architecture Behavioral of vga is

COMPONENT col_counter is 
    port(	clk: in STD_LOGIC;
        reset_n: in STD_LOGIC; 
		ctrl: in STD_LOGIC;
		col_number: out unsigned(9 downto 0);
		col_roll: out STD_LOGIC);
end COMPONENT;

COMPONENT row_counter is 
    port(	clk: in STD_LOGIC;
        reset_n: in STD_LOGIC; 
		ctrl: in STD_LOGIC;
		row_number: out unsigned(9 downto 0);
		row_roll: out STD_LOGIC);
end COMPONENT;

COMPONENT scopeFace is
    Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
		   trigger_volt: in unsigned (9 downto 0);
		   trigger_time: in unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
           ch1: in std_logic;
           ch1_enb: in std_logic;
           ch2: in std_logic;
           ch2_enb: in std_logic);
end COMPONENT;

   --Input
   signal ctrl : std_logic := '1';

 	--Outputs
   signal row_ctrl : std_logic;
   signal col_roll: STD_LOGIC;
   signal row_roll: STD_LOGIC;
   signal row_number: unsigned (9 downto 0);
   signal col_number: unsigned (9 downto 0);
   signal signal_r : std_logic_vector(7 downto 0);
   signal signal_g : std_logic_vector(7 downto 0);
   signal signal_b : std_logic_vector(7 downto 0);
begin
     columns_counter: col_counter
         PORT MAP (
              clk => clk,
              reset_n => reset_n,
              ctrl => ctrl,
              col_number => col_number,
              col_roll => col_roll
            );
     rows_counter: row_counter
         PORT MAP (
              clk => clk,
              reset_n => reset_n,
              ctrl => row_ctrl,
              row_number => row_number,
              row_roll => row_roll
            );
      scope_face: scopeFace
         PORT MAP ( 
           row => row_number,
           column => col_number,
		   trigger_volt => trigger_volt,
		   trigger_time => trigger_time,
           r => signal_r,
           g => signal_g,
           b => signal_b,
           ch1 => ch1,
           ch1_enb => ch1_enb,
           ch2 => ch2,
           ch2_enb => ch2_enb
           );
process(clk)
begin
    -- glue logic for cascading counter
    row_ctrl <= col_roll and ctrl;
    
    -- Calculating h_sync value
    if (col_number < 656 or col_number >= 752) then
        h_sync <= '1';
    else 
        h_sync <= '0';
    end if;
    
    -- Calculating v_sync value
    if (row_number < 490 or row_number >=492) then
        v_sync <= '1';
    else 
        v_sync <= '0';
    end if;
    
    -- Calculating blank value
    if (col_number >= 640 or row_number >=480) then
        blank <= '1';
    else 
        blank <= '0';
    end if;
    
    row <= row_number;
    column <= col_number;
    r <= signal_r;
    g <= signal_g;
    b <= signal_b;
end process;

end Behavioral;


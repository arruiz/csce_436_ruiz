----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--        lines in all of your code
----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        2/22/2021
-- Course:      CSCE 436
-- File:        lec12_cu.vhdl
-- HW:          Homework 8
-- Purp:        Contol unit for the lecture 12 FSM
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lec12_cu is
	Port(	clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			kbClk: in std_logic;
			cw: out STD_LOGIC_VECTOR(2 downto 0);
			sw: in STD_LOGIC;
			busy: out std_logic);
end lec12_cu;    

architecture Behavioral of lec12_cu is
    type state_type is (WaitStart, Init, Comp, While1, Shift, While0, Inc);
	signal state: state_type;	
begin
    ------------------------------------------------------
    --      bit 0
    --      status word
    --      1 - head to WaitStart 
    --      0 - continue
    ------------------------------------------------------
    
state_process: process(clk,reset)
begin
    if (rising_edge(clk)) then
        if (reset = '0') then 
            state <= WaitStart;
        else
            case state is
                when WaitStart =>
                    if (kbClk = '1') then state <= WaitStart;
                    elsif (kbClk = '0') then state <= Init; end if;
                when Init =>
                    state <= Comp;
                when Comp =>
                    if (sw = '1') then state <= WaitStart;
                    elsif (sw = '0') then state <= While1; end if;
                when While1 =>
                    if (kbClk = '1') then state <= While1;
                    elsif (kbClk = '0') then state <= Shift; end if;
                when Shift =>
                    state <= While0;
                when While0 =>
                    if (kbClk = '0') then state <= While0;
                    elsif (kbClk = '1') then state <= Inc; end if;
                when Inc =>
                    state <= Comp;
            end case;
        end if;
    end if;
end process;

	------------------------------------------------------------------------------
	--			OUTPUT EQUATIONS CONTROL WORD (cw)
	--	
	--		bit 2			         bit 1,0							
	--		busy			         counter							
	--		1-receieving signal	     00 hold		
	--		0-waiting on signal	     01 count up		
	--						         10 N/A	
	--						         11 reset	
	------------------------------------------------------------------------------	
	
cw <= "100" when state = Shift else
    "001" when state = Inc else
    "011" when state = WaitStart else
    "000"; -- All other states

busy <= '0' when state = WaitStart else
    '1'; -- when state is not WaitStart
end Behavioral;
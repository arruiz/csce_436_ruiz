----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--		lines in all of your code
----------------------------------------------------------------------------------
-- Name:	Andres Ruiz
-- Date:	Spring 2021
-- Course: 	CSCE 436
-- File: 	converter_tb .vhdl
-- HW:		Homework 2
-- Purp:	test bank for converter
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity converter_tb is 
end entity converter_tb;

architecture behavior of converter_tb is

	----------------------------------------------------------------------
	-- These signal names must match the names of the I/O markers
	----------------------------------------------------------------------
	component converter
        port(	i0, i1, i2, i3, i4, i5, i6, i7 :	in std_logic; 
					o0, o1, o2, o3:   		out std_logic);
	end component;
  
  
	signal s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12: std_logic;
	signal testVector: std_logic_vector(7 downto 0);
	CONSTANT TEST_ELEMENTS:integer:=10;
	SUBTYPE INPUT is std_logic_vector(7 downto 0);
	TYPE TEST_INPUT_VECTOR is array (1 to TEST_ELEMENTS) of INPUT;
	SIGNAL TEST_IN: TEST_INPUT_VECTOR := (	"01000101", "00010110", "00011110", "00100110", "00101001", "00101110", "00110110", "00111101", "00111110", "01000110");

    SUBTYPE OUTPUT is std_logic_vector(3 downto 0);
	TYPE TEST_OUTPUT_VECTOR is array (1 to TEST_ELEMENTS) of OUTPUT;
	SIGNAL TEST_OUT: TEST_OUTPUT_VECTOR := ("0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001");

	SIGNAL i : integer;		

begin

	----------------------------------------------------------------------
	-- Instantiate the Unit Under Test (UUT)
	----------------------------------------------------------------------
	UUT:	converter port map (
	   i0 => s1,
	   i1 => s2,
	   i2 => s3,
	   i3 => s4,
	   i4 => s5,
	   i5 => s6,
	   i6 => s7,
	   i7 => s8,
	   o0 => s9,
	   o1 => s10,
	   o2 => s11,
	   o3 => s12);
	   
        s1 <= testVector(0);
        s2 <= testVector(1);
        s3 <= testVector(2);
        s4 <= testVector(3);
        s5 <= testVector(4);
        s6 <= testVector(5);
        s7 <= testVector(6);
        s8 <= testVector(7);
        
		
	process
	begin
	
	for i in 1 to TEST_ELEMENTS loop
		-----------------------------------------
		-- Parse out the bits of the test_vector
		-----------------------------------------
		testVector <= test_in(i);
		wait for 10 ns; 
		assert (s12, s11, s10, s9) = test_out(i)
 				report "Error with input " & integer'image(i) & " in majority circuit "
				severity failure;
	end loop;
	
	---------------------------
	-- Halt the simulator
	---------------------------
    std.env.finish;
			
	end process tb;

end architecture behavior;
----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--		lines in all of your code
----------------------------------------------------------------------------------
-- Name:	Andres Ruiz
-- Date:	Spring 2021
-- Course: 	CSCE 436
-- File: 	converter.vhdl
-- HW:		Homework 2
-- Purp:	convert scancodes from 8 bit hex to 4 bit keyboard key
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity converter is
        port(	i0, i1, i2, i3, i4, i5, i6, i7: in std_logic; 
		o0, o1, o2, o3	: out std_logic);
end converter;

architecture behavior of converter is
    signal scancode: std_logic_vector(7 downto 0);
    signal keyboard_key: std_logic_vector(3 downto 0);
    
begin
	scancode <= i7 & i6 & i5 & i4 & i3 & i2 & i1 & i0;
	
	keyboard_key <= "0000" when scancode = "01000101" else
		"0001" when scancode = "00010110" else
		"0010" when scancode = "00011110" else
		"0011" when scancode = "00100110" else
		"0100" when scancode = "00101001" else
		"0101" when scancode = "00101110" else
		"0110" when scancode = "00110110" else
		"0111" when scancode = "00111101" else
		"1000" when scancode = "00111110" else
		"1001" when scancode = "01000110";
		
	(o3, o2, o1, o0) <= keyboard_key;

end behavior;
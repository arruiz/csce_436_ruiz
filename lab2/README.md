# Lab 2 - Data Acquisition, Storage and Display

## By Andres Ruiz

## Table of Contents (not required but makes things easy to read)
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose  

The purpose or this lab was to create an audio input and display it as a wave. To do this, I must create an audio codec (provided) and link that with the datapath file (created). After doing that, I must write the input data into BRAM (provided with modifications) to allow for future access. Next, I must create a control unit to determine the state of the program. To complete the lab, I will need to create the glue logic from the datapath to the control unit.  

### Preliminary design

#### Gate Check 1

For this initial gate check, I was assigned to link up our previous lab and ensure we were still able to display the scopeface and move the trigger values. This was a simple task of bringing the `Video.VHDL` file and all the files below it to the datapath and instantiating them there. The next thing step was to ensure the audio codec and make sure the audio input was functioning by adding a loopback process (provided). This was as simple as dropping the process into the datapath. 
[This Gate Check 1 Video](https://www.youtube.com/watch?v=yBUNGlMPMjU) shows that the default state of this lab was in working conditions. 

#### Gate Check 2

For the next gate check, I was assigned to create the BRAM and store values using a write address counter. Since the majority of the BRAM was given in a previous lecture, I create a `BRAM.VHDL` file to increase the program's readability. To write to the BRAM, I had to create a signal from the `L_bus_out` ouput from `audio_codec_wrapper.VHDL` to the `Din` input from `BRAM.VHDL`. I also created a counter for the write address that counted from 20 (where the grid begins) to 1023. For testing purposes, I defined the control word to always be `101` (Writing and counting). While the scopeface iterates over the columns, it checks the BRAM memory and determines if it should be writing the channel wave or not.
[This Gate Check 2 Video](https://www.youtube.com/watch?v=HnYWnriqW1A) shows that the BRAM is storing and reading values and that the channel wave is being activated. 

#### Post Gate Check Research
	
After the gate check, I wanted to look into the circuit to understand what was going on under the hood. I noticed that the `trigger_volt` was getting compared to two different values and being assigned to a status word, but I did not immedidately understand the logic. After looking into it, I realized the process stored the previous `L_bus_out` to compare to `trigger_volt` as well as comparing the current `L_bus_out` to `trigger_volt`. To better understand the logic, I drew a description of what was happening. 

![Trigger Logic Understanding](images/trigger_volt_logic_understanding.jpg)
###### Figure 1: Understanding the `trigger_volt` logic
	
### Software flow chart or algorithms

After completing the gate checks, the first set was to create the control unit for my system. To do this, I designed it on paper first to ensure that the system made sense before implementing it. Here is the state machine I designed: 

![FSM](images/lab2_fsm.jpg)
###### Figure 2: Designing the FSM

Figure 3 shows the architecture that was given to create Lab 2.

![Architecture](images/architecture.png)
###### Figure 3: Lab 2 Architecture

#### Pseudocode:
The following pseudocode explains when the `Trigger` status word should be enabled

```
IF ( current_Din > trigger_volt and previous_Din < trigger_volt ) then
	trigger = '1'
ELSE 
	trigger = '0'
```

The following pseudocode explains the loop that the system takes to determine when to increase the `write_cntr` and allow write access. Using this, I was able to design the FSM shown above.
```
WHILE ( 0 )
	IF ( trigger ) THEN
		write_counter = 0
		WHILE ( write_counter < 1023 ) 
			IF ( ready ) THEN
				write_counter = write_counter + 1
			END IF
		END LOOP
	END IF
END LOOP
```

#### Code:

The following code shows the FSM in the control unit: 

```
----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        2/23/2021
-- Course:      CSCE 436
-- File:        lab2_fsm.vhdl
-- HW:          Lab 2
-- Purp:        Control Unit for the lab 2 portion of the oscilloscope
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
...
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (reset_n = '0') then
				state <= TriggerWait;
			else 
				case state is 
					-- State that waits for the next Trigger signal
					when TriggerWait =>
						if (sw(Trigger) = '1') then state <= ReadyWait;
						else state <= TriggerWait; end if;
					-- State that waits for the next Ready signal (if at max, reset)
					when ReadyWait =>
						if (sw(ReadySW) = '1') and (sw(MaxCount) = '1') then state <= TriggerWait;
						elsif (sw(ReadySW) = '1') then state <= Counting;
						else state <= ReadyWait; end if;
					-- State that allows the datapath to read and write data
					when Counting =>
						state <= ReadyWait;
				end case;
			end if;
		end if;
	end process;
...
```
This FSM allows us to regulate when to increase the write counter and write memory.

This next code snippet shows the signed to unsigned conversion, the audio codec loopback, and the trigger assignment. 

```
	----------------------------------------------------------------------------------
	-- Name:        Andres Ruiz
	-- Date:        2/23/2021
	-- Course:      CSCE 436
	-- File:        lab2_datapath.vhdl
	-- HW:          Lab 2
	-- Purp:        Datapath entity stores the audio codec entity, video entity (from lab 1), 
	--              and the glue logic to connect the entities.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	----------------------------------------------------------------------------------
	
	...
	
    --------------------------------------------
    -- Audio Codec Loopback Process
    --------------------------------------------
    process (clk)
        begin
            if (rising_edge(clk)) then
                if reset_n = '0' then
                    L_bus_in <= (others => '0');
                    R_bus_in <= (others => '0');
                    previous_L_bus <= (others => '0');			
                elsif(ready = '1') then
                    L_bus_in <= L_bus_out;
                    R_bus_in <= R_bus_out;
                    -- Reassigning the previous L_bus to the current L_bus
                    previous_L_bus <= Din_Left;
                end if;
            end if;
    end process;
	
	...
	
	-----------------------------
	-- Din Signed to Unsigned Conversion
	-----------------------------
	unsigned_L_bus <= unsigned(L_bus_out) + 131072;
	unsigned_R_bus <= unsigned(R_bus_out) + 131072;

	...

	-----------------------------
	-- Status Word Assignments
	-----------------------------
	-- Checking if the bus values are passing the trigger volt line
	sw(Trigger) <= '1' when (((unsigned(Din_Left(17 downto 8)) - 292) > trigger_volt) and ((unsigned(previous_L_bus(17 downto 8)) - 292) < trigger_volt)) else
        '0';
		
	...

```

This code snippet contains a lot of important information (since the datapath contains the majority of the logic). First, the loopback process allowed me to test and ensure the audio codec was set up correctly. It also is where the `previous_L_bus` is assigned. This signal is needed for a comparison in the trigger signal. The second is the type change from signed to unsigned. This is necessary because the audio codec can give a range of positive and negative numbers (-131072 to 131071), however, our scopeface only has a positive range of values so we have to shift the value up 131071 so all the values of Din are positive. The last code snippet shows the assignment of the trigger. This logic was explained above. The shift by 292 is the same shift given to the readL and row comparison. This shift allows the wave to be centered on the scopeface. 

Figure 4 shows a testbench for the `L_bus_out` and `R_bus_out` datatype conversions. The reasoning is explained in the paragraph above.

![Datatype Conversion](images/conversion.png)
##### Figure 4: Datatype Conversion


### Hardware schematic

Figure 5 shows how we connect the board to an external display and an audio input. 

![Hardware Connection](images/hardware_connections.jpg)
##### Figure 5: Hardware Connections

### Debugging

During the two gate checks, there were not very many issues. One main roadblock I ran into was my loopback process was not working. After ensuring that the entity was linked into the datapath I knew it was an issue within the Audio Codec. After looking into the clocking wizard, I realized the output frequency was not correct. After reading through the Audio Codec file and updating the frequencies, the loopback process was properly working.

After the gate checks, I created the control unit. This process was fairly smooth, however, my wave would be scrolling across the screen. This issue came from multiple areas. For one, I was not saving the signed to unsigned conversion from the audio codec. This means I could be reading  negative number and comparing them to trigger volt. The next issue was I was not removing the bottom bits before the comparison. This means I was comparing a very large value to the trigger volt which would never activate the trigger signal. After fixing those issues, I was able to get the wave to lock based on the location of `trigger_volt`.

[This Debugging Video](https://www.youtube.com/watch?v=a6xO3xesWDI) shows the scrolling wave described above.

### Testing methodology or results

For testing, there are three separate test benches. One for the datapath, control unit, and flag register. By testing separately, we can ensure each individual part of the system is working. This allowed for easy debugging and a better understanding of the code. 

[This Results Video](https://www.youtube.com/watch?v=XJ-S4v5U7-g) shows the `trigger_volt` value deciding the wave position.

### Answers to Lab Questions

Here is the 500Hz wave I used to compute answers one and two:
![TestWave](images/obervation_wave.jpg)
##### Figure 6: Observation Wave
![TestWave](images/observation_1.jpg)
![TestWave](images/observation_2.jpg)
![TestWave](images/observation_3.jpg)
### Observations and Conclusions
During this lab, I was able to create a BRAM to store audio values and then display those values on the scopeface. Throughout I was able to understand the actual process the BRAM takes to store and obtain memory. Also, based on the knowledge from the previous lab, I created testbenches earlier to prevent errors. 

### Documentation
The architecture was given in the lab writeup. BRAM structure and information were given in lecture 13. The top-level entity and the audio codec wrapper were also given in the lab writeup.
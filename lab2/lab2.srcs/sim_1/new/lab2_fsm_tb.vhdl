----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        2/23/2021
-- Course:      CSCE 436
-- File:        lab2_fsm_tb.vhdl
-- HW:          Lab 2
-- Purp:        Testbench to ensure FSM functionality is correct.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY lab2_fsm_tb IS
END lab2_fsm_tb;
 
ARCHITECTURE behavior OF lab2_fsm_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT  lab2_fsm is
		Port(	clk: in  STD_LOGIC;
				reset_n : in  STD_LOGIC;
				sw: in STD_LOGIC_VECTOR(2 downto 0);
				cw: out STD_LOGIC_VECTOR(2 downto 0));
    END COMPONENT;
    
   signal clk : std_logic := '0';
   signal reset_n : std_logic := '0';
   signal sw : std_logic_vector(2 downto 0) := (others => '0');
   signal cw : std_logic_vector(2 downto 0) := (others => '0');
   -- Clock period definitions
   constant clk_period : time := 40 ns;
 
        -- Status Word Bit Representations
    constant MaxCount: integer := 0;
    constant Trigger: integer := 1;
    constant ReadySW: integer := 2;
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: lab2_fsm PORT MAP (
          clk => clk,
          reset_n => reset_n,
          sw => sw,
          cw => cw);

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;

 	------------------------------------------------------------------------------
	-- 		MEMORY INPUT EQUATIONS
	-- 
	--		bit 2				bit 1				   bit 0
	--		Ready		        Trigger	               MaxCount
	--		1-ready        	    1-cow				   0 - running
	--		0-not ready		    0-no cow			   1 - times up
 	------------------------------------------------------------------------------
 	
   -- Simulate Ready Signal
   ready_process_process :process
   begin
		sw(ReadySW) <= '0';
		wait for clk_period * 2;
		sw(ReadySW) <= '1';
		wait for clk_period;
   end process;
   
   -- Simulate Max Count Signal
   max_count_process :process
   begin
		sw(MaxCount) <= '0';
		wait for clk_period * 1023;
		sw(MaxCount) <= '1';
		wait for clk_period;
   end process;
   
   -- Simulate Trigger Signal
   trigger_process :process
   begin
		sw(Trigger) <= '0';
		wait for clk_period * 30;
		sw(Trigger) <= '1';
		wait for clk_period;
    end process;	

	reset_n <= '0', '1' after 1us;

END;
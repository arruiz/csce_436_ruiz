----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/03/2021 05:13:16 PM
-- Design Name: 
-- Module Name: flagRegister_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity flagRegister_tb is
end flagRegister_tb;

architecture Behavioral of flagRegister_tb is

COMPONENT flagRegister is
	Generic (N: integer := 8);
	Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			set, clear: in std_logic_vector(N-1 downto 0);
			Q: out std_logic_vector(N-1 downto 0));
end COMPONENT;

constant clk_period : time := 50 ns;
signal clk : std_logic := '0';
signal reset_n : std_logic := '0';
signal set, clear : std_logic_vector(7 downto 0) := (others => '0');
signal expected_Q : std_logic_vector(7 downto 0);
signal Q : std_logic_vector(7 downto 0);
begin
    uut: flagRegister
    Port Map( clk => clk,
              reset_n => reset_n,
              set => set,
              clear => clear,
              Q => Q
             );

   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;

    reset_n <= '0', '1' after 50 ns;
    set <= "00000000", "10101010" after 100 ns, "00000000" after 200 ns, "00001111" after 300 ns, "00000000" after 400 ns;
    clear <= "00000000", "00000000" after 100 ns, "11110000" after 200 ns, "11110000" after 300 ns, "11111111" after 400 ns;
    expected_Q <= "00000000", "10101010" after 125 ns, "00001010" after 225 ns, "00001111" after 325 ns, "00000000" after 425 ns;
end Behavioral;

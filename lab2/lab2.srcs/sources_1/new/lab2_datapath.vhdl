----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        2/23/2021
-- Course:      CSCE 436
-- File:        lab2_datapath.vhdl
-- HW:          Lab 2
-- Purp:        Datapath entity stores the audio codec entity, video entity (from lab 1), 
--              and the glue logic to connect the entities.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.lab2Parts.all;	
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab2_datapath is
    Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	tmds : out  STD_LOGIC_VECTOR (3 downto 0);
	tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
	sw: out std_logic_vector(2 downto 0);
	cw: in std_logic_vector (2 downto 0);
	btn: in	STD_LOGIC_VECTOR(4 downto 0);
	exWrAddr: in std_logic_vector(9 downto 0);
	exWen, exSel: in std_logic;
	Lbus_out, Rbus_out: out std_logic_vector(15 downto 0);
	exLbus, exRbus: in std_logic_vector(15 downto 0);
	flagQ: out std_logic_vector(7 downto 0);
	flagClear: in std_logic_vector(7 downto 0));
end lab2_datapath;

architecture Behavioral of lab2_datapath is

    -- Audio Signals
    signal ready: STD_LOGIC;
    signal L_bus_in : std_logic_vector(17 downto 0);
    signal R_bus_in : std_logic_vector(17 downto 0);
    signal L_bus_out : std_logic_vector(17 downto 0);
    signal R_bus_out : std_logic_vector(17 downto 0);
    
    -- Video Signals
    signal trigger_time: unsigned (9 downto 0);
    signal trigger_volt: unsigned (9 downto 0);
    signal row, column: unsigned(9 downto 0);
    signal old_button, button_activity: std_logic_vector(4 downto 0) := "00000";
    signal ch1_wave, ch2_wave: std_logic;
    signal ctrl: std_logic;
    
    -- BRAM Signals
    signal Left_DOUT: std_logic_vector(17 downto 0);
    signal Right_DOUT: std_logic_vector(17 downto 0);
    signal readL: unsigned(17 downto 0);
    signal readR: unsigned(17 downto 0);
    signal write_cntr: unsigned(9 downto 0);
    signal WRADDR: unsigned(9 downto 0);
    signal wrENB: STD_LOGIC;
    signal Din_Left: std_logic_vector(17 downto 0);
    signal Din_Right: std_logic_vector(17 downto 0);   
    signal unsigned_L_bus : unsigned(17 downto 0);
    signal unsigned_R_bus : unsigned(17 downto 0);
    signal vector_L_bus : std_logic_vector(17 downto 0);
    signal vector_R_bus : std_logic_vector(17 downto 0);
    signal previous_L_bus : std_logic_vector(17 downto 0);
    
    -- Flag register Signals 
    signal set : std_logic_vector(7 downto 0);
    signal clear : std_logic_vector(7 downto 0);
    signal Q : std_logic_vector(7 downto 0);
    
    -- Status Word Bit Representations
    constant MaxCount: integer := 0;
    constant Trigger: integer := 1;
    constant ReadySW: integer := 2;
    
    -- Control Word Bit Representations
    constant WriteEnable: integer := 2;
    constant Count_1: integer := 1;
    constant Count_0: integer := 0;
begin

audio_codec_wrapper_inst: Audio_Codec_Wrapper port map(
        clk => clk,
		reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        ready => ready,
        L_bus_in => L_bus_in,
        R_bus_in => R_bus_in,
        L_bus_out => L_bus_out,
        R_bus_out => R_bus_out,
        scl => scl,
        sda => sda);

Left_BRAM: Lab2Dual_dp port map(
        clk => clk,
        n_reset => reset_n,
        WRADDR => WRADDR,
        Din => Din_Left, -- modify to use the MUX
        WREN => wrENB,
        rENB => '1', -- Always Enabled
        RDADDR => column,
        DOUT  => Left_DOUT);
        
Right_BRAM: Lab2Dual_dp port map(
        clk => clk,
        n_reset => reset_n,
        WRADDR => WRADDR,
        Din => Din_Right, -- modify to use the MUX
        WREN => wrENB,
        rENB => '1', -- Always Enabled
        RDADDR => column,
        DOUT  => Right_DOUT);
        
video_inst: video port map( 
		clk => clk,
		reset_n => reset_n,
		tmds => tmds,
		tmdsb => tmdsb,
		trigger_time => trigger_time,
		trigger_volt => trigger_volt,
		row => row, 
		column => column,
		ch1 => ch1_wave,
		ch1_enb => ch1_wave,
		ch2 => ch2_wave,
		ch2_enb => ch2_wave); 
		
flag_register: flagRegister Port Map( 
              clk => clk,
              reset_n => reset_n,
              set => set,
              clear => clear,
              Q => Q
             );
             
    -----------------------------------------------------
    -- Counter for the BRAM write address
	-----------------------------------------------------
	--		The address counter sends in an address
	--		00			hold
	--		01			count up
	--		10			unused
	--		11			synch reset
	-----------------------------------------------------
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (reset_n = '0') then
				write_cntr <= "0000010100";
			-- Keep incrementing while count is less than 1025
			elsif (write_cntr < 1023 and cw(Count_1 downto Count_0) = "01") then
				write_cntr <= write_cntr + 1;
		    -- Checks if the coutner is at the max count; if so reset
			elsif (write_cntr = 1023 and cw(Count_1 downto Count_0) = "01") then
				write_cntr <= "0000010100";
			-- Reset if the control word says to
		    elsif (cw(Count_1 downto Count_0) = "11") then
				write_cntr <= "0000010100";
			end if;
		end if;
	end process;


    --------------------------------------------
    -- Audio Codec Loopback Process
    --------------------------------------------
    process (clk)
        begin
            if (rising_edge(clk)) then
                if reset_n = '0' then
                    L_bus_in <= (others => '0');
                    R_bus_in <= (others => '0');
                    previous_L_bus <= (others => '0');			
                elsif(ready = '1') then
                    L_bus_in <= L_bus_out;
                    R_bus_in <= R_bus_out;
                    -- Reassigning the previous L_bus to the current L_bus
                    previous_L_bus <= Din_Left;
                end if;
            end if;
    end process;
                
                
    --------------------------------------------
    -- Debouncing buttons
    --------------------------------------------
    process(clk)
        begin
            if (rising_edge(clk)) then
                if (reset_n = '0') then
                     --clear activity
                     button_activity <= "00000";
                else 
                     --set button activity
                     button_activity <= (old_button XOR btn) and btn;
                end if;
            old_button <= btn;
            end if;
    end process;
             
                
    --------------------------------------------
    -- Trigger Volt and Trigger Time Movement
    --------------------------------------------
    process(clk)
        begin
            if (rising_edge(clk)) then
                if (reset_n = '0') then
                     trigger_time <= to_unsigned(320,10);
                     trigger_volt <= to_unsigned(220,10);
                elsif(button_activity(0) = '1') then
                    trigger_volt <= trigger_volt - 10;
                elsif(button_activity(1) = '1') then
                    trigger_time <= trigger_time - 1;
                elsif(button_activity(2) = '1') then
                    trigger_volt <= trigger_volt + 10;
                elsif(button_activity(3) = '1') then
                    trigger_time <= trigger_time + 1;
                end if;
            end if;
    end process;
        
-----------------------------
-- Data type conversions
-----------------------------

-- Changing DOUT data type to unsigned for channel wave comparisons
readL <= unsigned(Left_DOUT);
readR <= unsigned(Right_DOUT);

-----------------------------
-- Din Signed to Unsigned Conversion
-----------------------------

unsigned_L_bus <= unsigned(L_bus_out) + 131072;
unsigned_R_bus <= unsigned(R_bus_out) + 131072;

vector_L_bus <= std_logic_vector(unsigned_L_bus);
vector_R_bus <= std_logic_vector(unsigned_R_bus); 
-----------------------------
-- External Select Logic
-----------------------------
-- Selecting the internal or external write address based of the external select ( Write Address select MUX )
WRADDR <= unsigned(exWrAddr) when exSel = '1' else
       write_cntr;

-- Assigning Write Enable bit based off external select
wrENB <= exWen when exSel = '1' else
       cw(WriteEnable);

 -- Assigning BRAM inputs based off external select
 Din_Left <= (exLBus & "00") when exSel = '1' else
        vector_L_bus;

 Din_Right <= (exRBus & "00") when exSel = '1' else
        vector_R_bus;
        
-----------------------------
-- Status Word Assignments
-----------------------------
-- Checking if the bus values are passing the trigger volt line
sw(Trigger) <= '1' when (((unsigned(Din_Left(17 downto 8)) - 292) > trigger_volt) and ((unsigned(previous_L_bus(17 downto 8)) - 292) < trigger_volt)) else
       '0';
       
-- Checking if the write counter is at the max
sw(MaxCount) <= '1' when write_cntr = 1023 else 
       '0';

-- Checking ready state from BRAM
sw(ReadySW) <= ready;

-----------------------------
-- Channel Wave Assignments
-----------------------------
ch1_wave <= '1' when ((readL(17 downto 8) - 292) = row) else
       '0';
       
ch2_wave <= '1' when ((readR(17 downto 8) - 292) = row) else
       '0';	

-----------------------------
-- Update output signals
-----------------------------
-- Updating trigger signals
trigger_volt <= trigger_volt;
trigger_time <= trigger_time;

-- Updating bus signals
L_bus_in <= L_bus_in;
R_bus_in <= R_bus_in;

end Behavioral;

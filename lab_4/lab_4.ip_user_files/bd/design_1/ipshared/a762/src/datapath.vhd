----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        4/7/2021
-- Course:      CSCE 436
-- File:        lab2_datapath.vhdl
-- HW:          Lab 4
-- Purp:        Datapath entity stores the audio codec entity and BRAM
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.lab4Parts.all;	
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab4_datapath is
    Port(
    -- ready: in std_logic;
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	sw: out std_logic_vector(1 downto 0);
	cw: in std_logic_vector (2 downto 0);
	btn: in	STD_LOGIC_VECTOR(4 downto 0);
	switch: in	STD_LOGIC_VECTOR(7 downto 0);
	Lbus_out, Rbus_out: out std_logic_vector(15 downto 0);
	frequency : in std_logic_vector(9 downto 0);
	exSel : in std_logic);
end lab4_datapath;

architecture Behavioral of lab4_datapath is

    -- Audio Signals
    signal ready: STD_LOGIC;
    signal L_bus_in : std_logic_vector(17 downto 0);
    signal R_bus_in : std_logic_vector(17 downto 0);
    signal L_bus_out : std_logic_vector(17 downto 0);
    signal R_bus_out : std_logic_vector(17 downto 0);
    signal L_out_ready : std_logic_vector(15 downto 0);
    signal R_out_ready : std_logic_vector(15 downto 0);
    signal old_button, button_activity: std_logic_vector(4 downto 0) := "00000";
    
    -- BRAM Signals
    signal Left_DOUT: std_logic_vector(15 downto 0);
    signal Right_DOUT: std_logic_vector(15 downto 0);

    signal read_cntr: unsigned(15 downto 0);

    signal Din_Left: std_logic_vector(17 downto 0);
    signal Din_Right: std_logic_vector(17 downto 0);   
    signal unsigned_L_bus : unsigned(17 downto 0);
    signal unsigned_R_bus : unsigned(17 downto 0);
    signal vector_L_bus : std_logic_vector(17 downto 0);
    signal vector_R_bus : std_logic_vector(17 downto 0);
   
    signal extendedFrequency : std_logic_vector(21 downto 0);
    
    signal phase: unsigned(15 downto 0);
    signal amplitude: unsigned(15 downto 0);
    
    signal leftOutModifiedAmplitude: std_logic_vector(31 downto 0);
    signal rightOutModifiedAmplitude: std_logic_vector(31 downto 0);
    
    signal selectBRAM: std_logic;
    -- Status Word Bit Representations
    constant ReadySW: integer := 0;
    constant MaxCount: integer := 1;
    
    -- Control Word Bit Representations
    constant Base_Plus_One: integer := 2;
    constant Count_1: integer := 1;
    constant Count_0: integer := 0;
begin

audio_codec_wrapper_inst: Audio_Codec_Wrapper port map(
        clk => clk,
		reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        ready => ready,
        L_bus_in => L_bus_in,
        R_bus_in => R_bus_in,
        L_bus_out => L_bus_out,
        R_bus_out => R_bus_out,
        scl => scl,
        sda => sda);

LeftBRAMSin: lab4_BRAM port map(
        clk => clk,
        n_reset => reset_n,
        readySignal => ready,
        RDADDR => read_cntr(15 downto 6),
        DOUT  => Left_DOUT);
        
RightBRAMSinCos: RightBRAMSineCos 
        port map(
        clk => clk,
        n_reset => reset_n,
        readySignal => ready,
        RDADDR => read_cntr(15 downto 6),
        DOUT  => Right_DOUT);
             
    -----------------------------------------------------
    -- Counter for the BRAM write address
	-----------------------------------------------------
	--		The address counter sends in an address
	--		00			hold
	--		01			count up
	--		10			unused
	--		11			synch reset
	-----------------------------------------------------
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (reset_n = '0') then
				read_cntr <= X"0000";
			-- Keep incrementing while count is less than 1025
			elsif (read_cntr < X"FFFF" and cw(Count_1 downto Count_0) = "01") then
				read_cntr <= read_cntr + phase;
		    -- Checks if the coutner is at the max count; if so reset
			elsif (read_cntr = X"FFFF" and cw(Count_1 downto Count_0) = "01") then
				read_cntr <= X"0000";
			-- Reset if the control word says to
		    elsif (cw(Count_1 downto Count_0) = "11") then
				read_cntr <= X"0000";
			end if;
		end if;
	end process;


    --------------------------------------------
    -- Audio Codec Loopback Process
    --------------------------------------------
    process (clk)
        begin
            if (rising_edge(clk)) then
                if reset_n = '0' then
                    L_bus_in <= (others => '0');
                    R_bus_in <= (others => '0');
                    leftOutModifiedAmplitude <= X"00000000";
                    rightOutModifiedAmplitude <= X"00000000";
                    L_out_ready <= X"0000";
                elsif(ready = '1') then                 
                    L_out_ready <= Left_DOUT;
                    R_out_ready <= Right_DOUT;
                    leftOutModifiedAmplitude <= std_logic_vector(amplitude * unsigned(L_out_ready));
                    rightOutModifiedAmplitude <= std_logic_vector(amplitude * unsigned(R_out_ready));
                    if (selectBRAM = '1') then
                        L_bus_in <= leftOutModifiedAmplitude(31 downto 14);
                    else 
                        R_bus_in <= rightOutModifiedAmplitude(31 downto 14);
                    end if;
                end if;
            end if;
    end process;
                
    --------------------------------------------
    -- Debouncing buttons
    --------------------------------------------
    process(clk)
        begin
            if (rising_edge(clk)) then
                if (reset_n = '0') then
                     --clear activity
                     button_activity <= "00000";
                else 
                     --set button activity
                     button_activity <= (old_button XOR btn) and btn;
                end if;
            old_button <= btn;
            end if;
    end process;
             
                
    --------------------------------------------
    -- Frequency and Amplitude Modification
    --------------------------------------------
    process(clk)
        begin
            if (rising_edge(clk)) then
                if (reset_n = '0') then
                     phase <= X"0259";
                     amplitude <= x"3fff";
                elsif(button_activity(0) = '1') then
                     amplitude <= amplitude + (unsigned(switch) * (2**4));
                elsif(button_activity(2) = '1') then
                     amplitude <= amplitude - (unsigned(switch) * (2**4));
                elsif(button_activity(4) = '1') then
                     selectBRAM <= not selectBRAM;   
                elsif(exSel = '1') then
                     extendedFrequency <= std_logic_vector(unsigned(frequency) * to_unsigned(87, 12));
                     phase <= unsigned(extendedFrequency(21 downto 6));
                elsif(button_activity(1) = '1') then
                     phase <= phase - unsigned(switch);
                elsif(button_activity(3) = '1') then
                     phase <= phase + unsigned(switch);             
                end if;
            end if;
    end process;
        
    sw(MaxCount) <= '1' when (read_cntr = X"FFFF") else
        '0';
    
    sw(ReadySW) <= ready;
    
end Behavioral;

# Lab 3 - Function Generation

## By Andres Ruiz

## Table of Contents (not required but makes things easy to read)
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose  

The purpose of this lab was to store a single wave into the BRAM and use a phase to adjust the speed at which the wave is read (frequency). Another part of the lab was to be able to modify the amplitude at which the audio is played at. The final step of the lab was to incorperate the UART and allow a user to input a desired frequency. 

### Preliminary design

#### Gate Check 1

The first gate check was designing the architecture. This required a lot of knowledge of what was happening behind the scene with the phase and the state diagram. The architecture designed below (Figure 1) has additional information to allow the user to use linear interpolation, however, that was not implemented inside my lab. Also, the state diagram designed below (Figure 2) was created to have an extra state for the bonus functionality. 

![Architecture](images/architecture.jpg)
##### Figure 1: Lab 4 Architecture

![State Diagram](images/state_diagram.jpg)
##### Figure 1: Lab 4 State Diagram
 

#### Gate Check 2

During gate check two, the goal was to add all the entities and connect them property. To ensure this, I made a testbench with the signals and also made sure that they were communicating properly. The waveform below shows values being read from the BRAM and modified by the desired amplitude. The waveform also shows the buttons modifying the amplitude and phase based off the switch value. 

![Waveform Button Press](images/waveformButtonPress.png)
##### Figure 3: Waveform with Button Presses

![Waveform BRAM Incrementing](images/waveformIncrementing.png)
##### Figure 4: Waveform with BRAM Incrementing

Figure 3 shows the ability to modify the amplitude and phase based off button presses and the switch states. Figure 4 shows the ability of the BRAM to increment based off the phase each ready signal.

### Software flow chart or algorithms

The flow charts used during this lab are discussed in the Gate Check 1 section and are Figure 1 and Figure 2.

I designed a program to take a sine, cosine, or combination of the two, and print the 64 initial values of BRAM. This program is located in the `generate` folder. This program can be executed using the command `gcc generateHex.c -lm`. This can be modified to print other waves by changing line `97` to any valid wave.  

Slave Register Map used during lab 4 implementation:

```
Slave Register Map
0 : Frequency
1 : External Select
```

#### Pseudocode:
The following pseudocode explains how to calculate the BRAM values at a specific value.

```
sinValue = sin(theta)
truncatedSin = (32768 * sinValue) + 32768
theta = theta + thetaIncrement
	
```
For a BRAM of 1024 values like the one used in this lab, theta is `3.14 / 512`


#### Code:

The code below shows how the output audio is synced using the ready signal inside the process. This ensures that the audio output only happens when the ready signal is high. 

`
----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        4/7/2021
-- Course:      CSCE 436
-- File:        lab2_datapath.vhdl
-- HW:          Lab 4
-- Purp:        Datapath entity stores the audio codec entity and BRAM
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 

	...
    process (clk)
        begin
            if (rising_edge(clk)) then
                if reset_n = '0' then
                    L_bus_in <= (others => '0');
                    R_bus_in <= (others => '0');
                    leftOutModifiedAmplitude <= X"00000000";
                    rightOutModifiedAmplitude <= X"00000000";
                    L_out_ready <= X"0000";
                elsif(ready = '1') then                 
                    L_out_ready <= Left_DOUT;
                    R_out_ready <= Right_DOUT;
                    leftOutModifiedAmplitude <= std_logic_vector(amplitude * unsigned(L_out_ready));
                    rightOutModifiedAmplitude <= std_logic_vector(amplitude * unsigned(R_out_ready));
                    if (selectBRAM = '1') then
                        L_bus_in <= leftOutModifiedAmplitude(31 downto 14);
                    else 
                        R_bus_in <= rightOutModifiedAmplitude(31 downto 14);
                    end if;
                end if;
            end if;
    end process;
	...

`
### Hardware schematic

Figure 5 shows how we output an audio, UART, and program the device.

![Hardware](images/hardware.jpg)
##### Figure 5: Hardware used for Lab 4

### Debugging

One issue I came accross while working on the lab was my BRAM was only being read for a couple ready signals then it would stop reading. The issue I had was my reset for the `read_cntr` was for a 10-bit value, however, the `read_cntr` was turned to a 16-bit value for the offset. Another issue I faced was when trying to implement the Microblaze. The issues were fairly small such as the Audio Codec Wrapper clock disappearing and forgetting to set signals to external. 

### Testing methodology or results

This lab was farily simple to test for. After creating the test bench, I was able to ensure the BRAM was being read properly and the button presses were modifying amplitude and phase accordingly. All of this testing was possible through the created testbench. As for testing with the UART, I made sure the user input was properly read by printing back after the user entered it. 

### Answers to Lab Questions

There are no lab questions for lab 4.

### Observations and Conclusions

Before this lab I always assumed that when loading preset values of data, you would read them in an accending order one at a time. However, after completing this lab, I understand there are many other ways to use the BRAM.

### Documentation
I used the lec_20.c file as a base file for creating `lab_4_uart.c`. I also used the given Audio Codec Wrapper given in previous labs. 
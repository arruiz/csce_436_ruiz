----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        2/23/2021
-- Course:      CSCE 436
-- File:        lab2_datapath_tb.vhdl
-- HW:          Lab 2
-- Purp:        Datapath for datapath to test the BRAM readings and ensure
--              signal values and types are their correct values.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
LIBRARY ieee; 
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.lab4Parts.all;		

entity lab4_tb is
end lab4_tb;

architecture Behavioral of lab4_tb is
    COMPONENT lab4
    Port ( clk : in  STD_LOGIC;
           ready : in std_logic;
           reset_n : in  STD_LOGIC;
		   ac_mclk : out STD_LOGIC;
		   ac_adc_sdata : in STD_LOGIC;
		   ac_dac_sdata : out STD_LOGIC;
		   ac_bclk : out STD_LOGIC;
		   ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;
		   tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
		   btn: in	STD_LOGIC_VECTOR(4 downto 0);
		   switch: in STD_LOGIC_VECTOR(7 downto 0));
	 end component;
	 
	 -- Input signals
	 signal clk: std_logic;
	 signal reset_n: std_logic;
	 signal btn: std_logic_vector(4 downto 0);
	 signal switch: std_logic_vector(7 downto 0);
	 signal ready: std_logic;
	 -- Output signals
	 signal ac_mclk: std_logic;
	 signal ac_adc_sdata: std_logic;
	 signal ac_dac_sdata: std_logic;
	 signal ac_bclk: std_logic;
	 signal ac_lrclk: std_logic;
	 signal scl: std_logic;
	 signal sda: std_logic;
	 signal tmds: std_logic_vector(3 downto 0);
	 signal tmdsb: std_logic_vector(3 downto 0);
	 constant clk_period : time := 500 ns;  -- Sets clock to ~ 100MHz
	 constant BIT_CLK_period : time := 40 ns;  -- Sets Bit Clock for Audio Codec to the necessary 25 MHz
begin

    uut: lab4 Port Map (
            clk => clk,
            ready => ready,
            reset_n => reset_n,
            ac_mclk => ac_mclk,
            ac_adc_sdata => ac_adc_sdata,
            ac_dac_sdata => ac_dac_sdata,
            ac_bclk => ac_bclk,
            ac_lrclk => ac_lrclk,
            scl => scl,
            sda => sda,
            tmds => tmds,
            tmdsb => tmdsb,
            btn => btn,
            switch => switch
            );
            
    clk_process :process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;
    
    ready_process: process
    begin
        ready <= '0';
        wait for clk_period * 8;
        ready <= '1';
        wait for clk_period * 2;
        ready <= '0';
        wait for clk_period * 6;
    end process;

    SDATA_process :process  -- Inputs alternating 1's and 0's on each Bit Clock
    begin
         ac_adc_sdata <= '0';
         wait for BIT_CLK_period;
         ac_adc_sdata <= '1';
         wait for BIT_CLK_period*5;
    end process;

    btn <= "00000", "00001" after 2us, "00000" after 3us, "00010" after 6us, "00000" after 7us, 
           "00100" after 10us, "00000" after 11us;

    switch <= "10101010";
    reset_n <= '0', '1' after 500ns;
end Behavioral;

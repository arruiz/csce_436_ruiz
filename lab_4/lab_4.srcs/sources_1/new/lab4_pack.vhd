----------------------------------------------------------------------------------
-- Name:        Andres Ruiz
-- Date:        2/23/2021
-- Course:      CSCE 436
-- File:        lab2_pack.vhdl
-- HW:          Lab 2
-- Purp:        Store all the components used in Lab 2.
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package lab4Parts is


--component lab4
--    Port ( clk : in  STD_LOGIC;
--           reset_n : in  STD_LOGIC;
--		   ac_mclk : out STD_LOGIC;
--		   ac_adc_sdata : in STD_LOGIC;
--		   ac_dac_sdata : out STD_LOGIC;
--		   ac_bclk : out STD_LOGIC;
--		   ac_lrclk : out STD_LOGIC;
--           scl : inout STD_LOGIC;
--           sda : inout STD_LOGIC;
--		   tmds : out  STD_LOGIC_VECTOR (3 downto 0);
--           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
--		   btn: in	STD_LOGIC_VECTOR(4 downto 0);
--		   switch: in STD_LOGIC_VECTOR(7 downto 0));
--end component;

--/////////////////////// Audio Codec Wrapper //////////////////////////////////--

component Audio_Codec_Wrapper 
    Port ( clk : in STD_LOGIC;
        reset_n : in STD_LOGIC;
        ac_mclk : out STD_LOGIC;
        ac_adc_sdata : in STD_LOGIC;
        ac_dac_sdata : out STD_LOGIC;
        ac_bclk : out STD_LOGIC;
        ac_lrclk : out STD_LOGIC;
        ready : out STD_LOGIC;
        L_bus_in : in std_logic_vector(17 downto 0); -- left channel input to DAC
        R_bus_in : in std_logic_vector(17 downto 0); -- right channel input to DAC
        L_bus_out : out  std_logic_vector(17 downto 0); -- left channel output from ADC
        R_bus_out : out  std_logic_vector(17 downto 0); -- right channel output from ADC
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC);
end component;


component lab4_fsm
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           sw: in std_logic_vector(1 downto 0);
		   cw: out std_logic_vector (2 downto 0));
end component;


component lab4_datapath
    Port ( -- ready : in std_logic;
           clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
		   ac_mclk : out STD_LOGIC;
		   ac_adc_sdata : in STD_LOGIC;
		   ac_dac_sdata : out STD_LOGIC;
		   ac_bclk : out STD_LOGIC;
		   ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;
           sw : out std_logic_vector(1 downto 0);
		   cw : in std_logic_vector(2 downto 0);
		   btn: in	STD_LOGIC_VECTOR(4 downto 0);
		   switch: in STD_LOGIC_VECTOR(7 downto 0);
		   Lbus_out: out std_logic_vector(15 downto 0);
		   Rbus_out: out std_logic_vector(15 downto 0));

end component;
	
component lab4_BRAM is
    Port(	clk: in  STD_LOGIC;
            n_reset : in  STD_LOGIC;
            readySignal : in STD_LOGIC;
            RDADDR : in unsigned(9 downto 0); 
            DOUT: out std_logic_vector(15 downto 0));
end component;

component RightBRAMSineCos is
	Port(	clk: in  STD_LOGIC;
			n_reset : in  STD_LOGIC;
			readySignal : in STD_LOGIC;
			RDADDR : in unsigned(9 downto 0); 
			DOUT: out std_logic_vector(15 downto 0));
end component;

end lab4Parts;
/*--------------------------------------------------------------------
-- Name:	Andres Ruiz
-- Date:	April 7, 2021
-- File:	lab_4_uart.c
-- Event:	Lab 4
-- Crs:		CSCE 436
--
-- Purp:	Link lab 4 with uart input
--
-- Documentation:	MicroBlaze Tutorial
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"
#include <stdlib.h>
#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out8 and its variations
#include <xil_exception.h>

/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for our audio PCORE
 */
#define audioBase				0x44a00000
#define audioFrequencyReg		audioBase			// 10 LSBs of slv_reg0 for frequency input
#define	audioExSelReg			audioBase+4			// 1 LSBs of slv_reg1 are external select

/*
 * The following constants define the Counter commands
 */
#define audio_EXTERNAL		0x01		// The external select on option
#define	audio_INTERNAL		0x00		// The external select off option

#define printf xil_printf			/* A smaller footprint printf */

#define	uartRegAddr			0x40600000		// read <= RX, write => TX

/************************** Function Prototypes ****************************/
void myISR(void);

/************************** Variable Definitions **************************/
/*
 * The following are declared globally so they are zeroed and so they are
 * easily accessible from a debugger
 */

int main(void) {

	unsigned char c;

	init_platform();

	print("Welcome to Lab 4 - Audio Generator\n\r");
	print("By: Andres Ruiz\n\r");
	print("Welcome to Lab 4\n\r");

    while(1) {

    	c=XUartLite_RecvByte(uartRegAddr);

		switch(c) {

    		/*-------------------------------------------------
    		 * Reply with the help menu
    		 *-------------------------------------------------
			 */
    		case '?':
    			printf("--------------------------\r\n");
    			printf("?: help menu\r\n");
    			printf("e:   Input an external frequency\r\n");
    			printf("i:   Board frequency modification\r\n");
    			printf("h:	 Additional functionality information");
    			printf("f:   flush terminal\r\n");
    			printf("--------------------------\r\n");
    			break;
			/*-------------------------------------------------
			 * Reads an external frequency
			 *-------------------------------------------------
			 */
    		case 'e': ;
    			unsigned char freq2;
    			unsigned char freq1;
    			unsigned char freq0;
    			unsigned int frequencyInteger;
    			Xil_Out8(audioExSelReg, 0x01);
    			printf("Enter a 3 digit value for frequency(ex: 123, 521, 091) followed by an enter\r\n");
    			// Reads a 3 digit input and stores them in 3 variables
    			freq2 = getchar( );
    			freq1 = getchar( );
    			freq0 = getchar( );
    			// Puts all the numbers into one string
    			char frequency[4] = {freq2, freq1, freq0, '\0'};
    			// Turns the string into an integer
    			frequencyInteger = atoi(frequency);
    			// Print the user input
    			printf("You entered: %d\r\n", frequencyInteger);
    			// Output frequency to microblaze
    			Xil_Out16(audioFrequencyReg, frequencyInteger);
    			break;
			/*-------------------------------------------------
			 * Switches to the internal controls
			 *-------------------------------------------------
			 */
			case 'i':
				Xil_Out8(audioExSelReg, audio_INTERNAL);
				break;
			/*-------------------------------------------------
			 * Prints additional information about the program
			 *-------------------------------------------------
			 */
			case 'h':
				printf("The external frequency can be an input in a range from 0 - 999.\r\n\n");
				printf("The internal frequency option uses the buttons and switches to \r\n");
				printf("modify the frequency and amplitude.\r\n\n");
				printf("Both options allow for switching the two channels (using the middle\r\n");
				printf("button) and modifying amplitude (top and bottom buttons based off\r\n");
				printf("the current value in the switches.\r\n\n");
				break;
			/*-------------------------------------------------
			 * Clear the terminal window
			 *-------------------------------------------------
			 */
            case 'f':
            	for (c=0; c<40; c++) printf("\r\n");
               	break;

			/*-------------------------------------------------
			 * Unknown character was
			 *-------------------------------------------------
			 */
    		default:
    			printf("unrecognized character: %c\r\n",c);
    			break;
    	} // end case

    } // end while 1

    cleanup_platform();

    return 0;
} // end main

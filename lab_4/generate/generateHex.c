#include <stdio.h>
#include <math.h>

char hexValues[1024][4];

void decToHexa(int n, int index)
{
    // char array to store hexadecimal number
    char hexaDeciNum[4];
 
    // counter for hexadecimal number array
    int i = 0;
    while (n != 0) {
        // temporary variable to store remainder
        int temp = 0;
 
        // storing remainder in temp variable.
        temp = n % 16;
 
        // check if temp < 10
        if (temp < 10) {
            hexaDeciNum[i] = temp + 48;
            i++;
        }
        else {
            hexaDeciNum[i] = temp + 55;
            i++;
        }
 
        n = n / 16;
    }
    char tempHexString[4];
    // printing hexadecimal number array in reverse order
    for (int j = i - 1; j >= 0; j--) {
        tempHexString[3 - j] = hexaDeciNum[j];
    }
    for (int k = 0; k < 4; k++) {
        hexValues[index][k] = tempHexString[k];
    }
}

void printInitVal(int n)
{
    // char array to store hexadecimal number
    char hexaDeciNum[2];
 
    // counter for hexadecimal number array
    int i = 0;
    while (n != 0) {
        // temporary variable to store remainder
        int temp = 0;
 
        // storing remainder in temp variable.
        temp = n % 16;
 
        // check if temp < 10
        if (temp < 10) {
            hexaDeciNum[i] = temp + 48;
            i++;
        }
        else {
            hexaDeciNum[i] = temp + 55;
            i++;
        }
 
        n = n / 16;
    }
    // printing hexadecimal number array in reverse order
    for (int j = i - 1; j >= 0; j--) {
        printf("%c", hexaDeciNum[j]);
    }
}

void printHexOrder(void) {
    int currentIndex = 0;
    for (int i = 0; i < 64; i++) {
        printf("INIT_");
        printInitVal(i);
        printf(" => X\"");
        for (int j = 15; j >= 0; j--) {
            currentIndex = 16 * i + j;
            for (int k = 0; k < 4; k++) {
                printf("%c", hexValues[currentIndex][k]);
            }
        }
        printf("\",");
        printf("\n");
    }
}

int main(void) {
    float theta = 0;
    float thetaIncrement = (3.14/512);
    float currentSine = 0;
    int truncatedSine = 0;
    for (int i = 0; i < 1024; i++) {
        currentSine = sin(theta);
        truncatedSine = (32768 * currentSine + 32768);
        theta += thetaIncrement;
        decToHexa(truncatedSine, i);
    }

    printHexOrder();
    return 0;
}

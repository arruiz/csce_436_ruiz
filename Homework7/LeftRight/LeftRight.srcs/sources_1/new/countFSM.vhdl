--------------------------------------------------------------------
-- Name:	Andres Ruiz
-- Date:	Friday, Februrary 18
-- File:	countFSM.vhdl
-- HW:		Homework 7
-- Crs:		CSCE 436
--
-- Purp:	Create an FSM to count the number of left presses followed by right presses
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity countFSM is
	Port(	clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			sw: in STD_LOGIC_VECTOR(1 downto 0);
			counter: out unsigned(2 downto 0));
end countFSM;



architecture behavior of countFSM is

	type state_type is (WaitLeft, L_Pressed, WaitRight, R_Pressed, Inc);
	signal state: state_type;
	
	constant leftButton: integer := 1;		-- helps keep status bits straight
	constant rightButton: integer := 0;
	
	signal count: unsigned(2 downto 0) := "000";
begin
	
	---------------------------------------------------------------------------------------------------
	-- 		MEMORY INPUT EQUATIONS
	-- 
	--		bit 3                      bit 2				       bit 1				      bit 0
	--		Left Button press          Left Button Release         Right Button press         Right Button Release
	--		1 - button pressed         1 - button released         1 - button pressed         1 - button released
	--		0 - button not pressed     0 - button not released     0 - button not pressed     0 - button not released
	---------------------------------------------------------------------------------------------------
    state_process: process(clk,reset)
	 begin
		if (rising_edge(clk)) then
			if (reset = '0') then 
				state <= WaitLeft;
			else
				case state is
					when WaitLeft =>
						if (sw(leftButton) = '1') then state <= L_Pressed; 
						elsif (sw(rightButton) = '1') then state <= WaitLeft;
						elsif (sw(leftButton) = '0') then state <= WaitLeft;
						elsif (sw(rightButton) = '0') then state <= WaitLeft; end if;
					when L_Pressed =>
						if (sw(rightButton) = '1') then state <= WaitLeft;
						elsif (sw(leftButton) = '0') then state <= WaitRight; end if;
					when WaitRight =>
						if (sw(leftButton) = '1') then state <= L_Pressed;	
						elsif (sw(rightButton) = '1') then state <= R_Pressed;	end if;		
					when R_Pressed =>
					    if (sw(leftButton) = '1') then state <= WaitLeft;
						elsif (sw(rightButton) = '0') then state <= Inc; end if;
					when Inc =>
					    count <= count + "1";
						state <= WaitLeft;				
				end case;
			end if;
		end if;
	end process;


	counter <= count;

end behavior;	
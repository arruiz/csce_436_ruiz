--------------------------------------------------------------------
-- Name:	Andres Ruiz
-- Date:	Friday, Februrary 18
-- File:	countFSM_tb.vhdl
-- HW:		Homework 7
-- Crs:		CSCE 436
--
-- Purp:	Testbench for the left-right counter (countFSM.vhdl)
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY countFSM_tb IS
END countFSM_tb;
 
ARCHITECTURE behavior OF countFSM_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT  countFSM is
		Port(	clk: in  STD_LOGIC;
				reset : in  STD_LOGIC;
				sw: in STD_LOGIC_VECTOR(1 downto 0);
				counter: out unsigned(2 downto 0));
    END COMPONENT;
    
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal sw : std_logic_vector(1 downto 0) := (others => '0');
   signal counter : unsigned(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 500 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: countFSM PORT MAP (
          clk => clk,
          reset => reset,
          sw => sw,
          counter => counter);

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
 
 	------------------------------------------------------------------------------
	-- 		MEMORY INPUT EQUATIONS
	-- 
	--		bit 2				bit 1				bit 0
	--		RFID scan		Cow Present		Timer Status
	--		1-cow checked	1-cow				0 - running
	--		0-waiting		0-no cow			1 - times up
 	------------------------------------------------------------------------------
	sw <= "00", "10" after 6us, "00" after 7us,  "10" after 8us, "00" after 9us,  "01" after 10us, "00" after 11us,
          "01" after 12us, "00" after 13us, "10" after 14us, "00" after 15us, "01" after 16us, "00" after 17us;

	reset <= '0', '1' after 1us;

END;
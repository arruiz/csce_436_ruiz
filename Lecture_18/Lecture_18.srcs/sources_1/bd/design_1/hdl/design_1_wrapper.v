//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Mon Mar  8 13:11:38 2021
//Host        : DESKTOP-8SKTBPP running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (DDR3_0_addr,
    DDR3_0_ba,
    DDR3_0_cas_n,
    DDR3_0_ck_n,
    DDR3_0_ck_p,
    DDR3_0_cke,
    DDR3_0_dm,
    DDR3_0_dq,
    DDR3_0_dqs_n,
    DDR3_0_dqs_p,
    DDR3_0_odt,
    DDR3_0_ras_n,
    DDR3_0_reset_n,
    DDR3_0_we_n,
    led_8bits_tri_io,
    reset,
    sys_clock,
    usb_uart_rxd,
    usb_uart_txd);
  output [14:0]DDR3_0_addr;
  output [2:0]DDR3_0_ba;
  output DDR3_0_cas_n;
  output [0:0]DDR3_0_ck_n;
  output [0:0]DDR3_0_ck_p;
  output [0:0]DDR3_0_cke;
  output [1:0]DDR3_0_dm;
  inout [15:0]DDR3_0_dq;
  inout [1:0]DDR3_0_dqs_n;
  inout [1:0]DDR3_0_dqs_p;
  output [0:0]DDR3_0_odt;
  output DDR3_0_ras_n;
  output DDR3_0_reset_n;
  output DDR3_0_we_n;
  inout [7:0]led_8bits_tri_io;
  input reset;
  input sys_clock;
  input usb_uart_rxd;
  output usb_uart_txd;

  wire [14:0]DDR3_0_addr;
  wire [2:0]DDR3_0_ba;
  wire DDR3_0_cas_n;
  wire [0:0]DDR3_0_ck_n;
  wire [0:0]DDR3_0_ck_p;
  wire [0:0]DDR3_0_cke;
  wire [1:0]DDR3_0_dm;
  wire [15:0]DDR3_0_dq;
  wire [1:0]DDR3_0_dqs_n;
  wire [1:0]DDR3_0_dqs_p;
  wire [0:0]DDR3_0_odt;
  wire DDR3_0_ras_n;
  wire DDR3_0_reset_n;
  wire DDR3_0_we_n;
  wire [0:0]led_8bits_tri_i_0;
  wire [1:1]led_8bits_tri_i_1;
  wire [2:2]led_8bits_tri_i_2;
  wire [3:3]led_8bits_tri_i_3;
  wire [4:4]led_8bits_tri_i_4;
  wire [5:5]led_8bits_tri_i_5;
  wire [6:6]led_8bits_tri_i_6;
  wire [7:7]led_8bits_tri_i_7;
  wire [0:0]led_8bits_tri_io_0;
  wire [1:1]led_8bits_tri_io_1;
  wire [2:2]led_8bits_tri_io_2;
  wire [3:3]led_8bits_tri_io_3;
  wire [4:4]led_8bits_tri_io_4;
  wire [5:5]led_8bits_tri_io_5;
  wire [6:6]led_8bits_tri_io_6;
  wire [7:7]led_8bits_tri_io_7;
  wire [0:0]led_8bits_tri_o_0;
  wire [1:1]led_8bits_tri_o_1;
  wire [2:2]led_8bits_tri_o_2;
  wire [3:3]led_8bits_tri_o_3;
  wire [4:4]led_8bits_tri_o_4;
  wire [5:5]led_8bits_tri_o_5;
  wire [6:6]led_8bits_tri_o_6;
  wire [7:7]led_8bits_tri_o_7;
  wire [0:0]led_8bits_tri_t_0;
  wire [1:1]led_8bits_tri_t_1;
  wire [2:2]led_8bits_tri_t_2;
  wire [3:3]led_8bits_tri_t_3;
  wire [4:4]led_8bits_tri_t_4;
  wire [5:5]led_8bits_tri_t_5;
  wire [6:6]led_8bits_tri_t_6;
  wire [7:7]led_8bits_tri_t_7;
  wire reset;
  wire sys_clock;
  wire usb_uart_rxd;
  wire usb_uart_txd;

  design_1 design_1_i
       (.DDR3_0_addr(DDR3_0_addr),
        .DDR3_0_ba(DDR3_0_ba),
        .DDR3_0_cas_n(DDR3_0_cas_n),
        .DDR3_0_ck_n(DDR3_0_ck_n),
        .DDR3_0_ck_p(DDR3_0_ck_p),
        .DDR3_0_cke(DDR3_0_cke),
        .DDR3_0_dm(DDR3_0_dm),
        .DDR3_0_dq(DDR3_0_dq),
        .DDR3_0_dqs_n(DDR3_0_dqs_n),
        .DDR3_0_dqs_p(DDR3_0_dqs_p),
        .DDR3_0_odt(DDR3_0_odt),
        .DDR3_0_ras_n(DDR3_0_ras_n),
        .DDR3_0_reset_n(DDR3_0_reset_n),
        .DDR3_0_we_n(DDR3_0_we_n),
        .led_8bits_tri_i({led_8bits_tri_i_7,led_8bits_tri_i_6,led_8bits_tri_i_5,led_8bits_tri_i_4,led_8bits_tri_i_3,led_8bits_tri_i_2,led_8bits_tri_i_1,led_8bits_tri_i_0}),
        .led_8bits_tri_o({led_8bits_tri_o_7,led_8bits_tri_o_6,led_8bits_tri_o_5,led_8bits_tri_o_4,led_8bits_tri_o_3,led_8bits_tri_o_2,led_8bits_tri_o_1,led_8bits_tri_o_0}),
        .led_8bits_tri_t({led_8bits_tri_t_7,led_8bits_tri_t_6,led_8bits_tri_t_5,led_8bits_tri_t_4,led_8bits_tri_t_3,led_8bits_tri_t_2,led_8bits_tri_t_1,led_8bits_tri_t_0}),
        .reset(reset),
        .sys_clock(sys_clock),
        .usb_uart_rxd(usb_uart_rxd),
        .usb_uart_txd(usb_uart_txd));
  IOBUF led_8bits_tri_iobuf_0
       (.I(led_8bits_tri_o_0),
        .IO(led_8bits_tri_io[0]),
        .O(led_8bits_tri_i_0),
        .T(led_8bits_tri_t_0));
  IOBUF led_8bits_tri_iobuf_1
       (.I(led_8bits_tri_o_1),
        .IO(led_8bits_tri_io[1]),
        .O(led_8bits_tri_i_1),
        .T(led_8bits_tri_t_1));
  IOBUF led_8bits_tri_iobuf_2
       (.I(led_8bits_tri_o_2),
        .IO(led_8bits_tri_io[2]),
        .O(led_8bits_tri_i_2),
        .T(led_8bits_tri_t_2));
  IOBUF led_8bits_tri_iobuf_3
       (.I(led_8bits_tri_o_3),
        .IO(led_8bits_tri_io[3]),
        .O(led_8bits_tri_i_3),
        .T(led_8bits_tri_t_3));
  IOBUF led_8bits_tri_iobuf_4
       (.I(led_8bits_tri_o_4),
        .IO(led_8bits_tri_io[4]),
        .O(led_8bits_tri_i_4),
        .T(led_8bits_tri_t_4));
  IOBUF led_8bits_tri_iobuf_5
       (.I(led_8bits_tri_o_5),
        .IO(led_8bits_tri_io[5]),
        .O(led_8bits_tri_i_5),
        .T(led_8bits_tri_t_5));
  IOBUF led_8bits_tri_iobuf_6
       (.I(led_8bits_tri_o_6),
        .IO(led_8bits_tri_io[6]),
        .O(led_8bits_tri_i_6),
        .T(led_8bits_tri_t_6));
  IOBUF led_8bits_tri_iobuf_7
       (.I(led_8bits_tri_o_7),
        .IO(led_8bits_tri_io[7]),
        .O(led_8bits_tri_i_7),
        .T(led_8bits_tri_t_7));
endmodule

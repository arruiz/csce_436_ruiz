----------------------------------------------------------------------------------
-- Comments start with two dashes
-- You should always have the following
--		lines in all of your code
----------------------------------------------------------------------------------
-- Name:	Andres Ruiz
-- Date:	Spring 2021
-- Course: 	CSCE 436
-- File: 	significantOne.vhd
-- HW:		Homework 1
-- Purp:	find the most significant one in a hex value
--
-- Doc:	<list the names of the people who you helped>
-- 		<list the names of the people who assisted you>
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;						-- These lines are similar to a #include in C
use IEEE.std_logic_1164.all;

entity significantOne is
        port(	i0, i1, i2, i3:	in std_logic; 
					o0, o1:   		out std_logic);
end significantOne;

architecture structure of significantOne is
signal	s1: std_logic;	-- wires which begin and end in the component

begin 
	s1 <= i1 and not i2 and not i3;
	o0 <= s1 or i3;
	o1 <= i2 or i3;

end structure;
 
